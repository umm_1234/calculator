# Copyright (C) 2021 Jaroslav Prokop <jar.prokop@volny.cz>

# This file is part of Umm calculator.

# Umm calculator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Umm calculator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Umm calculator.  If not, see <https://www.gnu.org/licenses/>.
require 'calculator/gui/dialog'

module Calculator
  module GUI
    ##
    # +HelpDialog+ is responsible for opening the user documentation.
    #
    # @author Jaroslav Prokop
    class HelpDialog
      # Path to the user documentation PDF.
      USER_DOC_PATH = '../dokumentace.pdf'.freeze

      def initialize; end

      ##
      # Open the user documentation based on the path in +USER_DOC_PATH+.
      def execute
        system("xdg-open #{USER_DOC_PATH}")
      end
    end
  end
end
