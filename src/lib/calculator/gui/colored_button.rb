# Copyright (C) 2021 Jaroslav Prokop <jar.prokop@volny.cz>

# This file is part of Umm calculator.

# Umm calculator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Umm calculator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Umm calculator.  If not, see <https://www.gnu.org/licenses/>.
require 'fox16'
require 'fox16/colors'

module Calculator
  module GUI
    ##
    # +ColoredButton+ creates a button with default color.
    #
    # @author Jaroslav Prokop
    class ColoredButton < Fox::FXButton
      include Fox

      ##
      # Creates new button with specified options.
      #
      # @param frame [Fox::FXFrame]
      # @param label [String]
      # @param kwargs [Hash] Additional keyword arguments to be supplied.
      # @yield [colored_button] Yield block with the created instance of ColoredButton.
      # @see https://larskanis.github.io/fxruby/Fox/FXButton.html FxRuby button documentation on
      #   available options.
      def initialize(frame, label, **kwargs, &block)
        color = kwargs[:color] || Fox::FXColor::WhiteSmoke
        kwargs.delete :color

        pro = proc do |button|
          button.borderColor = color
          button.backColor = color
          block.call(button) if block_given?
        end

        super frame, label, **kwargs, &pro
      end
    end
  end
end
