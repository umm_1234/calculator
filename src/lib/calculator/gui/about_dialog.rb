# Copyright (C) 2021 Jaroslav Prokop <jar.prokop@volny.cz>

# This file is part of Umm calculator.

# Umm calculator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Umm calculator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Umm calculator.  If not, see <https://www.gnu.org/licenses/>.
require 'calculator/gui/dialog'

module Calculator
  module GUI
    ##
    # About Dialog wrapper.
    #
    # @author Jaroslav Prokop
    class AboutDialog < Calculator::GUI::Dialog
      # @param parent [Fox::FXComposite] parent frame
      def initialize(parent)
        super parent, 'about', ABOUT_TEXT
      end

      # Constant containing the about message.
      ABOUT_TEXT = <<~ABOUT.freeze
        Calculator made in Ruby using fxruby library.

        Authors: Jaroslav Prokop, Jan Valušek, Milan Tichavský, Ondřej Podroužek

        This software is licensed under the GPLv3 License.

        See the license packaged with the software.
      ABOUT
    end
  end
end
