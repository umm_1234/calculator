# Copyright (C) 2021 Jaroslav Prokop <jar.prokop@volny.cz>

# This file is part of Umm calculator.

# Umm calculator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Umm calculator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Umm calculator.  If not, see <https://www.gnu.org/licenses/>.
require 'calculator/gui/dsl'

module Calculator
  module GUI
    ##
    # Parent class for constructing dialogs
    #
    # @author Jaroslav Prokop
    class Dialog
      include Calculator::GUI::DSL
      include Fox

      # Message box instance.
      attr_reader :mbox

      ##
      # Creates message box and puts it into the `mbox` variable.
      #
      # @param parent [Fox::FXComposite] Parent frame for message box.
      # @param label [String] Message box title.
      # @param content [String] Message box text content.
      def initialize(parent, label, content)
        @mbox = message_box(nil, parent, label, content, MBOX_OK | DECOR_TITLE | DECOR_BORDER, nil)
      end
    end
  end
end
