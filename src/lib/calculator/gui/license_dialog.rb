# Copyright (C) 2021 Jaroslav Prokop <jar.prokop@volny.cz>

# This file is part of Umm calculator.

# Umm calculator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Umm calculator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Umm calculator.  If not, see <https://www.gnu.org/licenses/>.
require 'calculator/gui/dsl'

module Calculator
  module GUI
    ##
    # Class that constructs License dialog with license text.
    #
    # @author Jaroslav Prokop
    class LicenseDialog < Fox::FXDialogBox
      include Calculator::GUI::DSL
      include Fox

      # Path to license text. We presume that it was run with `make run` in development.
      # Default path should be changed before installing to proper location.
      LICENSE_PATH = '../LICENSE.txt'.freeze

      # rubocop: disable Metrics/AbcSize

      # @param parent [Fox::FXComposite] Parent frame.
      def initialize(parent)
        super(parent, 'Calculator License', DECOR_TITLE | DECOR_BORDER | DECOR_RESIZE,
              0, 0, 0, 0, 6, 6, 6, 6, 4, 4)
        @delegation_table = {}
        begin
          license = File.read(LICENSE_PATH)
        rescue Errno::ENOENT
          license = "License could not be found at '#{LICENSE_PATH}'!"
        end

        horizontal_frame(:closebox, self,
                         opts: LAYOUT_SIDE_BOTTOM | LAYOUT_FILL_X | PACK_UNIFORM_WIDTH)

        colored_button(nil, closebox, 'Ok', target: self, selector: FXDialogBox::ID_ACCEPT,
                                            opts: LAYOUT_RIGHT | FRAME_RAISED | FRAME_THICK)
        # Editor part
        horizontal_frame(:edit_box, self,
                         opts: LAYOUT_SIDE_TOP | LAYOUT_FILL_X |
                         LAYOUT_FILL_Y | FRAME_SUNKEN | FRAME_THICK)

        text_area(:license_text_area, edit_box, nil, 0,
                  opts: TEXT_READONLY | TEXT_WORDWRAP | LAYOUT_FILL_X | LAYOUT_FILL_Y)

        license_text_area.visibleRows = 50
        license_text_area.visibleColumns = 60
        # Fill with license text
        license_text_area.text = license
      end
      # rubocop: enable Metrics/AbcSize
    end
  end
end
