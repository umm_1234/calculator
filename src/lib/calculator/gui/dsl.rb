# Copyright (C) 2021 Jaroslav Prokop <jar.prokop@volny.cz>

# This file is part of Umm calculator.

# Umm calculator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Umm calculator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Umm calculator.  If not, see <https://www.gnu.org/licenses/>.
require 'fox16'
require 'fox16/colors'
require 'calculator/gui/colored_button'

module Calculator
  module GUI
    # rubocop: disable Metrics/ModuleLength
    # rubocop: disable Metrics/ParameterLists

    ##
    # module providing a variation of Domain specific language to write GUI with optional name
    # definition.
    #
    # @author Jaroslav Prokop
    module DSL
      include Fox

      ##
      # Exception that gets raised when 2 same symbols get added into the lookup table.
      class VariableRedefiningError < StandardError
        attr_reader :variable_symbol

        ##
        # @param msg [String] Message of the exception.
        # @param variable_symbol [Symbol] The duplicate symbol.
        def initialize(msg, variable_symbol)
          super(msg)
          @variable_symbol = variable_symbol
        end
      end

      ##
      # Maps +name+ to the +object+ in the +@delegation_table+ lookup table.
      #
      # If +name+ is +nil+, the addition of object to delegation table is skipped.
      #
      # @param name [Symbol] Symbol to identify the object in the delegation_table.
      # @param object [Object] Object to assign to symbol.
      # @return [Object] Object that was passed in the +object+ parameter.
      # @raise [VariableRedefiningError] if +name+ is already present in the lookup table.
      def add_to_lookup(name, object)
        return object if name.nil?

        unless @delegation_table[name].nil?
          raise DSL::VariableRedefiningError.new(
            'Variable already present in lookup table.',
            name
          )
        end

        @delegation_table[name] = object
      end

      ##
      # Construct +Fox::FXHorizontalFrame+ and add it to lookup table if +name+ is not nil.
      #
      # @param name [Symbol or nil] Object identifier.
      # @param frame [Fox::FXComposite] Parent frame.
      # @param opts [Hash] Keyword arguments containning additional arguments.
      # @return [Fox::FXHorizontalFrame] Initialized horizontal frame.
      # @yield [horizontal_frame]
      # @see https://larskanis.github.io/fxruby/Fox/FXHorizontalFrame.html Fox::FXHorizontalFrame
      #   documentation
      def horizontal_frame(name, frame, **opts)
        add_to_lookup(
          name,
          Fox::FXHorizontalFrame.new(frame, **opts) do |fr|
            yield fr if block_given?
          end
        )
      end

      ##
      # Construct +Fox::FXVerticalFrame+ and add it to lookup table if +name+ is not nil.
      #
      # @param name [Symbol or nil] Object identifier.
      # @param frame [Fox::FXComposite] Parent frame.
      # @param opts [Hash] Keyword arguments containning additional arguments.
      # @return [Fox::FXVerticalFrame] Initialized vertical frame.
      # @yield [vertical_frame]
      # @see https://larskanis.github.io/fxruby/Fox/FXVerticalFrame.html Fox::FXVerticalFrame
      #   documentation
      def vertical_frame(name, frame, **opts)
        add_to_lookup(
          name,
          Fox::FXVerticalFrame.new(frame, **opts) do |fr|
            yield fr if block_given?
          end
        )
      end

      ##
      # Construct +Fox::FXMatrix+ and add it to lookup table if +name+ is not nil.
      #
      # @param name [Symbol or nil] Object identifier.
      # @param frame [Fox::FXComposite] Parent frame.
      # @param cols [Integer] Number of max columns or rows in matrix. Depends on layout options.
      # @param opts [Hash] Keyword arguments containning additional arguments.
      # @return [Fox::FXMatrix] Initialized matrix.
      # @yield [matrix]
      # @see https://larskanis.github.io/fxruby/Fox/FXMatrix.html Fox::FXMatrix
      #   documentation
      def matrix(name, frame, cols, **opts)
        add_to_lookup(
          name,
          Fox::FXMatrix.new(frame, cols, **opts) do |mat|
            yield mat if block_given?
          end
        )
      end

      ##
      # Construct +Fox::FXSpring+ and add it to lookup table if +name+ is not nil.
      #
      # @param name [Symbol or nil] Object identifier.
      # @param frame [Fox::FXComposite] Parent frame.
      # @param layout [Integer] Layout options.
      # @param opts [Hash] Keyword arguments containning additional arguments.
      # @return [Fox::FXSpring] Initialized spring.
      # @yield [spring]
      # @see https://larskanis.github.io/fxruby/Fox/FXSpring.html Fox::FXSpring
      #   documentation
      def spring(name, frame, layout, **opts)
        add_to_lookup(
          name,
          Fox::FXSpring.new(frame, layout, **opts) do |spr|
            yield spr if block_given?
          end
        )
      end

      ##
      # Construct +Fox::FXTextField+ and add it to lookup table if +name+ is not nil.
      #
      # @param name [Symbol or nil] Object identifier.
      # @param frame [Fox::FXComposite] Parent frame.
      # @param cols [Integer] Max number of characters in a line.
      # @param data_target [Fox::FXDataTarget] Data target for the text field.
      # @param data_msg_identifier [Integer] Message identifier.
      # @param opts [Hash] Keyword arguments containning additional arguments..
      # @return [Fox::FXTextField] Initialized text field.
      # @yield [text_field]
      # @see https://larskanis.github.io/fxruby/Fox/FXTextField.html Fox::FXTextField
      #   documentation
      def text_field(name, frame, cols, data_target = nil, data_msg_identifier = 0, **opts)
        add_to_lookup(
          name,
          Fox::FXTextField.new(frame, cols, data_target, data_msg_identifier, **opts) do |field|
            yield field if block_given?
          end
        )
      end

      ##
      # Construct +Fox::FXText+ and add it to lookup table if +name+ is not nil.
      #
      # @param name [Symbol or nil] Object identifier.
      # @param frame [Fox::FXComposite] Parent frame.
      # @param data_target [Fox::FXDataTarget] Data target for the text field.
      # @param data_msg_identifier [Integer] Message identifier.
      # @param opts [Hash] Keyword arguments containning additional arguments.
      # @return [Fox::FXText] Initialized text object.
      # @yield [text]
      # @see https://larskanis.github.io/fxruby/Fox/FXText.html Fox::FXText documentation.
      def text_area(name, frame, data_target, data_msg_identifier, **opts)
        add_to_lookup(
          name,
          Fox::FXText.new(frame, data_target, data_msg_identifier, **opts) do |field|
            yield field if block_given?
          end
        )
      end

      ##
      # Construct +Calculator::GUI:ColoredButton+ and add it to lookup table if +name+ is not nil.
      #
      # @param name [Symbol or nil] Object identifier.
      # @param frame [Fox::FXComposite] Parent frame.
      # @param label [String] Label of the button.
      # @param opts [Hash] Keyword arguments containning additional arguments.
      # @return [Calculator::GUI:ColoredButton] Initialized colored button object.
      # @yield [colored_button]
      # @see Calculator::GUI::ColoredButton Calculator::GUI::ColoredButton documentation.
      def colored_button(name, frame, label, **opts)
        add_to_lookup(
          name,
          Calculator::GUI::ColoredButton.new(frame, label, **opts) do |button|
            yield button if block_given?
          end
        )
      end

      ##
      # Construct +Fox::FXStatusBar+ and add it to lookup table if +name+ is not nil.
      #
      # @param name [Symbol or nil] Object identifier.
      # @param frame [Fox::FXComposite] Parent frame.
      # @param opts [Hash] Keyword arguments containning additional arguments.
      # @return [Fox::FXStatusBar] Initialized status bar object.
      # @yield [status_bar]
      # @see https://larskanis.github.io/fxruby/Fox/FXStatusBar.html Fox::FXStatusBar documentation.
      def status_bar(name, frame, opts)
        add_to_lookup(
          name,
          Fox::FXStatusBar.new(frame, opts) do |bar|
            yield bar if block_given?
          end
        )
      end

      ##
      # Construct +Fox::FXDataTarget+ and add it to lookup table if +name+ is not nil
      #
      # The initial value will be set to the value of +initial_val+
      #
      # @param name [Symbol or nil] Object identifier,
      # @param initial_val [String] initial value that is set in the data target.
      # @return [Fox::FXDataTarget] Initialized data target object.
      # @see https://larskanis.github.io/fxruby/Fox/FXDataTarget.html
      #   Fox::FXDataTarget documentation.
      def data_target(name, initial_val)
        add_to_lookup(
          name,
          Fox::FXDataTarget.new(initial_val)
        )
      end

      ##
      # Construct +Fox::FXMenuBar+ and add it to lookup table if +name+ is not nil.
      #
      # @param name [Symbol or nil] Object identifier.
      # @param frame [Fox::FXComposite] Parent frame.
      # @param layout [Integer] Layout options.
      # @return [Fox::FXMenuBar] Initialized menu bar object.
      # @yield [menu_bar]
      # @see https://larskanis.github.io/fxruby/Fox/FXMenuBar.html Fox::FXMenuBar documentation.
      def menu_bar(name, frame, layout)
        add_to_lookup(
          name,
          Fox::FXMenuBar.new(frame, layout) do |bar|
            yield bar if block_given?
          end
        )
      end

      ##
      # Construct +Fox::FXMenuPane+ and add it to lookup table if +name+ is not nil.
      #
      # @param name [Symbol or nil] Object identifier.
      # @param frame [Fox::FXComposite] Parent frame.
      # @return [Fox::FXMenuPane] Initialized menu pane object.
      # @yield [menu_pane]
      # @see https://larskanis.github.io/fxruby/Fox/FXMenuPane.html Fox::FXMenuPane documentation.
      def menu_pane(name, frame)
        add_to_lookup(
          name,
          Fox::FXMenuPane.new(frame) do |pane|
            yield pane if block_given?
          end
        )
      end

      ##
      # Construct +Fox::FXMenuCommand+ and add it to lookup table if +name+ is not nil.
      #
      # @param name [Symbol or nil] Object identifier.
      # @param frame [Fox::FXComposite] Parent frame.
      # @param label [String] Label of the menu command.
      # @param target [Fox::FXDataTarget] Data target for the text field.
      # @param message_identifier [Integer] Integer identifying the message.
      # @param opts [Hash] Keyword arguments containning additional arguments.
      # @return [Fox::FXMenuCommand] Initialized menu command object.
      # @yield [menu_command]
      # @see https://larskanis.github.io/fxruby/Fox/FXMenuCommand.html
      #   Fox::FXMenuCommand documentation.
      def menu_command(name, frame, label, target, message_identifier, opts)
        add_to_lookup(
          name,
          Fox::FXMenuCommand.new(frame, label, nil, target, message_identifier, opts) do |command|
            yield command if block_given?
          end
        )
      end

      ##
      # Construct +Fox::FXMenuTitle+ and add it to lookup table if +name+ is not nil.
      #
      # @param name [Symbol or nil] Object identifier.
      # @param frame [Fox::FXComposite] Parent frame.
      # @param label [String] Label of the menu title.
      # @param popup_menu [Fox::FXPopup] Popup menu to attach menu title to.
      # @param icon [Pathname] Path to icon.
      # @return [Fox::FXMenuTitle] Initialized menu title object.
      # @yield [menu_title]
      # @see https://larskanis.github.io/fxruby/Fox/FXMenuTitle.html
      #   Fox::FXMenuTitle documentation.
      def menu_title(name, frame, label, popup_menu, icon = nil)
        add_to_lookup(
          name,
          Fox::FXMenuTitle.new(frame, label, icon, popup_menu) do |menu_title|
            yield menu_title if block_given?
          end
        )
      end

      ##
      # Construct +Fox::FXPopup+ and add it to lookup table if +name+ is not nil.
      #
      # @param name [Symbol or nil] Object identifier.
      # @param frame [Fox::FXComposite] Parent frame.
      # @return [Fox::FXPopup] Initialized popup object.
      # @yield [popup]
      # @see https://larskanis.github.io/fxruby/Fox/FXPopup.html
      #   Fox::Fox::FXPopup documentation.
      def popup(name, frame)
        add_to_lookup(
          name,
          Fox::FXPopup.new(frame) do |popup|
            yield popup if block_given?
          end
        )
      end

      ##
      # Construct +Fox::FXSplitter+ and add it to lookup table if +name+ is not nil.
      #
      # @param name [Symbol or nil] Object identifier.
      # @param frame [Fox::FXComposite] Parent frame.
      # @param target [Fox::FXObject] The message target for this splitter
      # @param selector [Integer] Message identifier.
      # @param opts [Hash] Keyword arguments containning additional arguments.
      # @return [Fox::FXSplitter] Initalized splitter object.
      # @yield [splitter]
      # @see https://larskanis.github.io/fxruby/Fox/FXSplitter.html
      #   Fox::FXSplitter documentation.
      def splitter(name, frame, target, selector, **opts)
        add_to_lookup(
          name,
          Fox::FXSplitter.new(frame, target, selector, **opts) do |split|
            yield split if block_given?
          end
        )
      end

      # Construct +Fox::FXOption+ and add it to lookup table if +name+ is not nil.
      #
      # @param name [Symbol or nil] Object identifier.
      # @param popup [Fox::FXPopup] Popup parent class to attach this to.
      # @param target [Fox::FXObject] Message target for FXOption.
      # @param opts [Hash] Keyword arguments containning additional arguments.
      # @return [Fox::FXOption] Initialized option object.
      # @yield [option_menu]
      # @see https://larskanis.github.io/fxruby/Fox/FXOption.html
      #   Fox::FXOption documentation.
      def option(name, popup, target, **opts)
        add_to_lookup(
          name,
          Fox::FXOption.new(popup, target, **opts) do |split|
            yield split if block_given?
          end
        )
      end

      # Construct +Fox::FXOptionMenu+ and add it to lookup table if +name+ is not nil.
      #
      # @param name [Symbol or nil] Object identifier.
      # @param frame [Fox::FXComposite] Parent frame.
      # @param popup [Fox::FXPopup] Popup parent class to attach this to.
      # @param opts [Hash] Keyword arguments containning additional arguments.
      # @return [Fox::FXOptionMenu] Initialized option menu object.
      # @yield [option_menu]
      # @see https://larskanis.github.io/fxruby/Fox/FXOptionMenu.html
      #   Fox::FXOptionMenu documentation.
      def option_menu(name, frame, popup, **opts)
        add_to_lookup(
          name,
          Fox::FXOptionMenu.new(frame, popup, **opts) do |opt_men|
            yield opt_men if block_given?
          end
        )
      end

      ##
      # Construct +Fox::FXMessageBox+ and add it to lookup table if +name+ is not nil.
      #
      # @param name [Symbol or nil] Object identifier.
      # @param frame [Fox::FXComposite] Parent frame.
      # @param title [String] Message box label.
      # @param text [String] Message box text content.
      # @param layout [Integer] Layout options.
      # @param icon [Pathname] Path to icon.
      # @return [Fox::FXMessageBox] Initialized message box object.
      # @see https://larskanis.github.io/fxruby/Fox/FXMessageBox.html
      #   Fox::FXMessageBox documentation.
      def message_box(name, frame, title, text, layout, icon = nil)
        add_to_lookup(
          name,
          Fox::FXMessageBox.new(frame, title, text, icon, layout)
        )
      end

      ##
      # Display a message box with given +label+ and +text+.
      #
      # @param label [String] Box label.
      # @param text [String] Box text message.
      # @return [nil]
      # @see Calculator::GUI::DSL State box creates message box using message_box.
      def state_box(label, text)
        message_box(
          nil, self, label, text,
          MBOX_OK | DECOR_TITLE | DECOR_BORDER, nil
        ).execute
        nil
      end

      ##
      # Overload +method_missing+ to also look at +@delegation_table+.
      def method_missing(meth, *args, &block)
        if @delegation_table&.include? meth.to_sym
          @delegation_table[meth.to_sym]
        else
          super meth, *args, &block
        end
      end

      ##
      # Overload +respond_to_missing?+ to also look inside the +@delegation_table+
      def respond_to_missing?(meth, include_private = false)
        @delegation_table&.include?(meth.to_sym) || super
      end
    end
    # rubocop: enable Metrics/ModuleLength
    # rubocop: enable Metrics/ParameterLists
  end
end
