# Copyright (C) 2021 Jan Valušek
# Copyright (C) 2021 Ondřej Podroužek

# This file is part of Umm calculator.

# Umm calculator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Umm calculator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Umm calculator.  If not, see <https://www.gnu.org/licenses/>.

require 'bigdecimal'

module Calculator
  # Handles basic and complex math operations
  # @author Ondřej Podroužek, Jan Valušek
  class Math
    # Raised when a given numerical value is out of range
    class MathError < RangeError
      attr_reader :string

      def initialize(msg = 'MathError')
        super(msg)
      end
    end

    # @!group Basic operations

    # Adds two numbers together
    # @param a_num
    # @param b_num
    # @return [Numeric] addition of a_num and b_num
    # @example (1+1)
    #   require 'calculator/math'
    #   math = Calculator::Math.new
    #   math.addition(1,1) #=> 2
    def addition(a_num, b_num)
      a_num + b_num
    end

    # Subtracts one number from another
    # @param a_num
    # @param b_num
    # @return [Numeric] subtraction of b_num from a_num
    # @example (2-1)
    #   require 'calculator/math'
    #   math = Calculator::Math.new
    #   math.subtraction(2,1) #=> 1
    def subtraction(a_num, b_num)
      a_num - b_num
    end

    # Multiplies two numbers
    # @param a_num
    # @param b_num
    # @return [Numeric] multiplication of a_num and b_num
    # @example (2*2)
    #   require 'calculator/math'
    #   math = Calculator::Math.new
    #   math.multiplication(2,2) #=> 4
    def multiplication(a_num, b_num)
      a_num * b_num
    end

    # Divides one number by another
    # @param a_num
    # @param b_num
    # @return [Numeric] division of a_num by b_num
    # @raise [Error] b_num is zero
    # @example (3/2)
    #   require 'calculator/math'
    #   math = Calculator::Math.new
    #   math.division(3,2) #=> 1.5
    def division(a_num, b_num)
      raise MathError, "Division by zero in: '#{a_num} / #{b_num}'" if b_num.zero?

      a_num / b_num
    end

    # Performs modulo operation.
    #
    # Make sure you understand how modulo works for negative numbers (take a look at test cases).
    # @param numerator [Numeric or BigDecimal]
    # @param denominator [Numeric or BigDecimal]
    # @example (3%2)
    #   require 'calculator/math'
    #   math = Calculator::Math.new
    #   math.modulo(5,3) #=> 2
    def modulo(numerator, denominator)
      raise MathError, 'Modulo with zero denominator is illegal.' if denominator.zero?

      numerator % denominator
    end

    # Negates `num`
    # @param num [Numeric or BigDecimal]
    # @return [Numeric] division of a_num by b_num
    #
    # @example (-5)
    #   require 'calculator/math'
    #   math = Calculator::Math.new
    #   math.unary_minus(5) #=> -5
    def unary_minus(num)
      - num
    end

    # @!endgroup
    # @!group Complex operations

    # Counts factorial
    # @param num [Integer or BigDecimal] the number the factorial is counted from,
    #   domain: integers from [0, INFINITY)
    # @return [Numeric] the factorial of num
    # @raise [MathError] num is out of factorial domain
    # @example Count factorial of 5
    #   require 'calculator/math'
    #   math = Calculator::Math.new
    #   math.factorial(5) #=> 120
    def factorial(num)
      if num.negative?
        raise Calculator::Math::MathError,
              "factorial: The processed number can't be negative."
      end

      unless (num.is_a? Integer) || ((num.is_a? BigDecimal) && num.frac.zero?)
        raise Calculator::Math::MathError,
              'factorial: The processed number must be integer.'
      end

      if num.is_a? Integer
        (1..num).inject(1) { |sum, n| multiplication(sum, n) }
      else
        (1..num).inject(BigDecimal(1.to_s)) { |sum, n| multiplication(sum, n) }
      end
    end

    # Raises the number to a defined power
    # @param base [Numeric] the base of exponentiation,
    #   domain: (-INFINITY, INFINITY)
    # @param power [Integer or BigDecimal] the power of exponentiation,
    #   domain: (-INFINITY, INFINITY)
    # @return [Numeric] the base raised to the power
    # @example Raise 2 to 3
    #   require 'calculator/math'
    #   math = Calculator::Math.new
    #   math.exponentiation(2, 3) #=> 8
    def exponentiation(base, power)
      base**power
    end

    # Counts degree's root of number
    # @param num [Numeric] the number to count the root of,
    #   domain: (-INFINITY, INFINITY) with odd degree, [0, INFINITY) with even degree
    # @param degree [Integer or BigDecimal] the degree of the root,
    #   domain: (0, INFINITY)
    # @return [Numeric] the degree's root of number
    # @raise [MathError] degree is negative
    # @raise [MathError] num is out of domain
    # @example Count 2nd root of 25
    #   require 'calculator/math'
    #   math = Calculator::Math.new
    #   math.root(25, 2) #=> 5
    def root(num, degree)
      unless degree.positive?
        raise Calculator::Math::MathError,
              'root: The degree must be greater than zero.'
      end

      if num.negative? && (degree % 2).zero?
        raise Calculator::Math::MathError,
              "root: The number can't be negative when even degree is set."
      end

      return 0 if num.zero?

      result = ->(n, d) { exponentiation(n, exponentiation(d, -1)) }
      if num.negative?
        -result.call(-num, degree)
      else
        result.call(num, degree)
      end
    end

    # Counts the logarithm of number to base
    # @param num [Numeric] the number to logarithm,
    #   domain: (0, INFINITY)
    # @param base [Numeric] the base of logarithm,
    #   domain: (0, 1) U (1, INFINITY)
    # @return [Numeric] the logarithm of number to base
    # @raise [MathError] base equals to one
    # @raise [MathError] base is lower than or equals to zero
    # @raise [MathError] num is out of the logarithm's domain
    # @example Count logarithm of 8 to base 2
    #   require 'calculator/math'
    #   math = Calculator::Math.new
    #   math.Math.log(8, 2) #=> 3
    def log(num, base)
      if base == 1
        raise Calculator::Math::MathError,
              "log: The base can't be equal to one."
      end

      unless num.positive?
        raise Calculator::Math::MathError,
              'log: The number must be greater than zero.'
      end

      unless base.positive?
        raise Calculator::Math::MathError,
              'log: The base must be greater than zero.'
      end

      ::Math.log(num, base)
    end
    # @!endgroup
  end
end
