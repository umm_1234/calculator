# Copyright (C) 2021 Jaroslav Prokop <jar.prokop@volny.cz>

# This file is part of Umm calculator.

# Umm calculator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Umm calculator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Umm calculator.  If not, see <https://www.gnu.org/licenses/>.
require 'calculator/gui/about_dialog'
require 'calculator/gui/colored_button'
require 'calculator/gui/dsl'
require 'calculator/gui/license_dialog'
require 'calculator/gui/help_dialog'

module Calculator
  # GUI module that grouping GUI elements together.
  # @author Jaroslav Prokop
  module GUI
  end
end
