# Copyright (C) 2021 Milan Tichavský <milantichavsky@seznam.cz>

# This file is part of Umm calculator.

# Umm calculator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Umm calculator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Umm calculator.  If not, see <https://www.gnu.org/licenses/>.

require 'calculator/parser'
require 'calculator/math'

module Calculator
  # Contains methods for solving postfix expressions.
  #
  # @example Solve expression given in posfix form
  #   require 'calculator/solver'
  #   postfix_expression = [BigDecimal('26'), BigDecimal('8'), Operation.new(:add)]
  #   solver = Solve.new(postfix_expression)
  #   solver.solve #=> BigDecimal('34')
  # @author Milan Tichavský
  class Solver
    # General exception
    class SolverError < StandardError; end

    # Postfix expression
    attr_reader :postfix_expr

    # Math library instance
    attr_reader :math_lib

    # @param postfix_expr [Array] postfix expression with Operations, Parentheses, BigDecimals
    def initialize(postfix_expr)
      @postfix_expr = postfix_expr
      @math_lib = Calculator::Math.new
    end

    # Solves a postfix expression.
    #
    # @example Illegal combination of operations and operands error
    #   # Operation multiplication needs two operands, got one
    #   require 'calculator/solver'
    #   postfix_expression = [BigDecimal('26'), Operation.new(:mul)]
    #   solver = Solve.new(postfix_expression)
    #   solver.solve #=> ParserError("Stack is empty but it should not be.")
    # @return [BigDecimal] result of the expression
    # @raise [ParserError] stack is empty = illegal combination of operations and operands
    # @raise [ParserError] invalid value of operands for given operation (e.g. dividing by zero)
    def solve
      stack = []

      @postfix_expr.each do |token|
        case token
        when BigDecimal
          stack.push(token)
        when Operation
          perform_operation(token.op, stack)
        else
          raise SolverError, 'Invalid token'
        end
      end
      stack.first
    end

    private

    # Performs operation on numbers on top of the stack.
    #
    # @param operation [Symbol] which operation should be performed
    # @param stack [Array] contains numbers on which the operation should be performed
    # @raise [ParserError] stack is empty
    # @raise [ParserError] invalid value of operands for given operation
    def perform_operation(operation, stack)
      case operation
      when :fct
        stack.push(factorial(stack))
      when :ums
        stack.push(unary_minus(stack))
      else
        stack.push(solve_binary_operation(stack, operation))
      end
    end

    # Performs unary minus operation on a number on top of the stack.
    #
    # @param stack [Array]
    # @return [BigDecimal] result
    # @raise [SolverError] stack is empty
    def unary_minus(stack)
      operand = stack.pop
      raise SolverError, 'Stack is empty but it should not be' if operand.nil?

      @math_lib.unary_minus(operand)
    end

    # Computes factorial for a number on top of the stack.
    #
    # @param stack [Array]
    # @return [BigDecimal] factorial result
    # @raise [SolverError] stack is empty
    # @raise [SolverError] when trying to compute factorial of non-integer or negative numer
    def factorial(stack)
      operand = stack.pop
      raise SolverError, 'Stack is empty but it should not be' if operand.nil?

      begin
        @math_lib.factorial(operand)
      rescue Math::MathError
        raise SolverError, 'Illegal factorial operand value'
      end
    end

    # Performs modulo operation.
    #
    # @param numerator [Numeric or BigDecimal]
    # @param denominator [Numeric BigDecimal]
    # @raise [SolverError] when trying to do modulo with zero denominator
    def modulo(numerator, denominator)
      @math_lib.modulo(numerator, denominator)
    rescue Math::MathError
      raise SolverError, 'Modulo with zero denominator is illegal.'
    end

    # Divides numerator by denumerator.
    #
    # @param numerator [Numeric or BigDecimal]
    # @param denumerator [Numeric or BigDecimal]
    # @raise [SolverError] when trying to divide by zero
    def division(numerator, denumerator)
      @math_lib.division(numerator, denumerator)
    rescue Math::MathError
      raise SolverError, 'Cannot divide by zero'
    end

    # Performs nth root.
    #
    # @param degree [Numeric or BigDecimal]
    # @param operand [Numeric or BigDecimal]
    # @raise [SolverError] illegal combination
    def root(degree, operand)
      @math_lib.root(operand, degree)
    rescue Math::MathError
      raise SolverError, 'Illegal combination of parameters'
    end

    # rubocop:disable Metrics/CyclomaticComplexity
    # rubocop: disable Metrics/MethodLength

    # Solves the expression by aplying operation to operands on top of the stack.
    #
    # @param stack [Array] stores temorary results as BigDecimals
    # @param operation [Symbol]
    # @return [BigDecimal] result of the expression
    # @raise [SolverError] stack is empty
    def solve_binary_operation(stack, operation)
      right = stack.pop
      left = stack.pop
      raise SolverError, 'Stack is empty but it should not be.' if left.nil? || right.nil?

      case operation
      when :add
        @math_lib.addition(left, right)
      when :sub
        @math_lib.subtraction(left, right)
      when :mul
        @math_lib.multiplication(left, right)
      when :div
        division(left, right)
      when :mod
        modulo(left, right)
      when :exp
        @math_lib.exponentiation(left, right)
      when :nrt
        root(left, right)
      else
        raise SolverError, 'Unknown operation'
      end
    end

    # rubocop: enable Metrics/MethodLength
    # rubocop:enable Metrics/CyclomaticComplexity
  end
end
