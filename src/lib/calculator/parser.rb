# Copyright (C) 2021 Milan Tichavský <milantichavsky@seznam.cz>

# This file is part of Umm calculator.

# Umm calculator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Umm calculator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Umm calculator.  If not, see <https://www.gnu.org/licenses/>.

require 'bigdecimal'

module Calculator
  # Wrapper around operations
  Operation = Struct.new(:op)

  # Wrapper around parentheses
  Parenthesis = Struct.new(:which)

  # Contains all expression parsing logic.
  #
  # Parsed numbers are converted to BigDecimals.
  # @example Transfrom string into posfix expression
  #   require 'calculator/parser'
  #   expression = '26 + 8'
  #   parser = Parser.new(expression)
  #   parser.tokenize.to_postfix.tokens #=> [BigDecimal('26'), BigDecimal('8'), Operation.new(:add)]
  # @author Milan Tichavský
  class Parser
    # Array of BigDecimals, Operations and Parentheses
    attr_reader :tokens

    # Operation precedence, meaning of the abbreviations:
    # fct: factorial
    # exp: exponentiation
    # nrt: nth root
    # ums: unary minus
    # mul: multiplication
    # div: division
    # mod: modulo
    # add: addition
    # sub: subtraction
    OP_PRECEDENCE = { fct: 5, exp: 4, nrt: 4, ums: 3, mul: 2, div: 2, mod: 2, add: 1,
                      sub: 1 }.freeze

    # Characters that are representing operations
    OP_REGEX = %r{[\-+*/%!^√r]}.freeze

    # Operation to symbol mapping for converting characters.
    OP_SYMBOLS = { '!' => :fct, '^' => :exp, '√' => :nrt, 'r' => :nrt, '*' => :mul, '/' => :div,
                   '%' => :mod, '-' => :sub, '+' => :add }.freeze

    # Holds regex matching known parentheses characters.
    PAR_REGEX = /[(){}\[\]]/.freeze

    # Parenthesis to symbol mapping for converting characters.
    PAR_SYMBOLS = { '(' => :left, ')' => :right, '{' => :left, '}' => :right, '[' => :left,
                    ']' => :right }.freeze

    # Regex matching all characters that can be used as separators in decimal numbers.
    NUM_SEPARATOR_REGEX = /[.,]/.freeze

    # General error
    class ParserError < StandardError; end

    # Raised when parentheses do not match.
    class ParenthesisMatchError < ParserError; end

    # Raised when the parser encounters character that it does not understand.
    class UnknownCharacter < ParserError
      attr_reader :char

      # @param char [Character] character that caused the problem
      def initialize(char, msg = 'Unknown character')
        super(msg)
        @char = char
      end
    end

    # Raised when a parsed number cannot be converted to BigDecimal.
    class NumberFormatError < ParserError
      attr_reader :char

      def initialize(char, msg = 'Cannot be converted to BigDecimal')
        super(msg)
        @char = char
      end
    end

    # @param expression [String] mathematical expression
    def initialize(expression)
      @expression = expression
    end

    # Splits `expression` into individual tokens.
    #
    # Unary expressions are taken care of in this function by `unary_substitution`. It could be
    # parsed immediately, but it would make code hard to understand.
    #
    # @return [Array] tokens
    # @raise [InvalidCharacter] expression contains a character that is not a valid token
    def tokenize
      @tokens = []
      number_buffer = ''

      # Goes character by character, it converts multiple digits into one number.
      @expression.each_char do |char|
        case char
        when /\d/
          number_buffer << char
        when NUM_SEPARATOR_REGEX
          number_buffer << '.'
        when Regexp.union(OP_REGEX, PAR_REGEX)
          handle_significant_char(char, number_buffer)
          number_buffer = ''
        when /[[:blank:]]/
          next
        else
          raise UnknownCharacter, char
        end
      end
      append_number(number_buffer)
      unary_substitution
      self
    end

    # Transforms tokens from infix into a postfix notation. Relevant link:
    #
    # https://runestone.academy/runestone/books/published/pythonds/BasicDS/InfixPrefixandPostfixExpressions.html#general-infix-to-postfix-conversion
    #
    # @raise [InvalidCharacter] when the parentheses do not match
    def to_postfix
      operation_stack = []
      postfix_expression = []

      @tokens.each do |token|
        case token
        when BigDecimal
          postfix_expression << token
        when Parenthesis.new(:left)
          operation_stack.push(token)
        when Parenthesis.new(:right)
          match_parentheses(operation_stack, postfix_expression)
        when Operation
          handle_operation(token, operation_stack, postfix_expression)
        else
          raise UnknownCharacter.new(token, 'Unknown token')
        end
      end
      append_remaining_operators(operation_stack, postfix_expression)
      @tokens = postfix_expression
      self
    end

    private

    # Appends number and then appends significant char (as structure).
    #
    # @param char [Character]
    # @param number_buffer [Array]
    # @raise [InvalidCharacter] content of the buffer could not be converted into `BigDecimal`
    # @raise [InvalidCharacter] char is not a known operation nor parenthesis
    def handle_significant_char(char, number_buffer)
      append_number(number_buffer)
      append_significant_char(char)
    end

    # Substitutes unary operations for their binary counterparts where appropriate.
    #
    # Acts on `@tokens`.
    def unary_substitution
      new_tokens = []
      operation = true
      @tokens.each do |token|
        case token
        when Operation
          operation = operation_substitution(new_tokens, operation, token)
        when Parenthesis
          new_tokens.append(token)
        else
          new_tokens.append(token)
          operation = false
        end
      end
      @tokens = new_tokens
    end

    # Substitutes unary minus where appropriate.
    #
    # Unary plus is dropped because it carries no meaning. Notice that factorial is in postfix by
    # default, threrefore it is treated differently.
    #
    # @param new_tokens [Array]
    # @param operation [Boolean]
    # @param token [Operation]
    # @return [Boolean] new value of operation
    def operation_substitution(new_tokens, operation, token)
      if token.op == :fct
        new_tokens.append(token)
        return false
      elsif token.op == :sub && operation == true
        new_tokens.append(Operation.new(:ums))
      elsif token.op == :add && operation == true
        # Don't append. Unary plus carries no meaning.
      else
        new_tokens.append(token)
        return true
      end
      operation
    end

    # Appends `number` to `tokens`.
    #
    # @param number [String]
    # @raise [InvalidCharacter] content of the buffer could not be converted into `BigDecimal`
    def append_number(number)
      return if number.empty?

      begin
        @tokens << BigDecimal(number)
      rescue ArgumentError
        raise NumberFormatError, number
      end
    end

    # Converts `char` to a structure corresponding to an operation or a parenthesis it represents
    # and appends it to tokens.
    #
    # @param char [Character] Operation or parenthesis
    # @raise [InvalidCharacter] char is not a known operation nor parenthesis
    def append_significant_char(char)
      case char
      when OP_REGEX
        @tokens.append(Operation.new(OP_SYMBOLS[char]))
      when PAR_REGEX
        @tokens.append(Parenthesis.new(PAR_SYMBOLS[char]))
      else
        raise UnknownCharacter.new(char, 'Unknown operation or parenthesis')
      end
    end

    # Appends operations from `operation_stack` to the `postfix_expression` until a matching left
    # parenthesis is found.
    #
    # @param operation_stack [Array]
    # @param postfix_expression [Array]
    # @raise [InvalidCharacter] when parentheses do not match
    def match_parentheses(operation_stack, postfix_expression)
      while (op = operation_stack.pop) != Parenthesis.new(:left)
        raise ParenthesisMatchError, 'Matching left parenthesis is missing' if op.nil?

        postfix_expression.append(op)
      end
    end

    # Appends operators of higher precedence (than `token`'s precedence) from `operation_stack`
    # to the `postfix_expression`. Then it pushes `token` onto the `operation_stack`.
    #
    # @param token [Char] current operator we want to add
    # @param operation_stack [Array] operators are saved here
    # @param postfix_expression [Array] holds the resulting postfix expression
    def handle_operation(token, operation_stack, postfix_expression)
      while !operation_stack.empty? && operation_stack[-1] != Parenthesis.new(:left) &&
            OP_PRECEDENCE[operation_stack[-1].op] >= OP_PRECEDENCE[token.op]

        postfix_expression << operation_stack.pop
      end
      operation_stack.push(token)
    end

    # Appends all remaining operators from `operation_stack` to the `postfix_expression`.
    #
    # @param operation_stack [Array]
    # @param postfix_expression [Array]
    # @raise [InvalidCharacter] when parentheses do not match
    def append_remaining_operators(operation_stack, postfix_expression)
      until operation_stack.empty?
        token = operation_stack.pop
        if token == Parenthesis.new(:left)
          raise ParenthesisMatchError, 'Matching right parenthesis is missing'
        end

        postfix_expression << token
      end
    end
  end
end
