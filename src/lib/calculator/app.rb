# Copyright (C) 2021 Jaroslav Prokop <jar.prokop@volny.cz>

# This file is part of Umm calculator.

# Umm calculator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Umm calculator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Umm calculator.  If not, see <https://www.gnu.org/licenses/>.

# GUI requires
require 'calculator/gui'

# Equation solving
require 'calculator/parser'
require 'calculator/solver'
require 'calculator/translator'

module Calculator
  # rubocop: disable Metrics/ClassLength

  ##
  # Calculator::App contains contruction logic for GUI.
  # @author Jaroslav Prokop
  class App < Fox::FXMainWindow
    include Calculator::GUI::DSL
    include Responder

    # Parentheses mapping for buttons.
    PARENTHESES = [
      { left: '(', right: ')' },
      { left: '[', right: ']' },
      { left: '{', right: '}' }
    ].freeze

    ID_ABOUT,
    ID_CLEAR_HISTORY,
    ID_HELP,
    ID_LICENSE = enum(Fox::FXMainWindow::ID_LAST, 47)

    # rubocop: disable Metrics/MethodLength
    # rubocop: disable Metrics/AbcSize
    # rubocop: disable Metrics/BlockLength

    ##
    # @param app [Fox::FXApp]
    def initialize(app)
      super(app, 'Umm Calculator',
            width: 600, height: 800,
            opts: LAYOUT_FILL_Y | LAYOUT_FILL_X | DECOR_ALL)
      @delegation_table = {}

      FXMAPFUNC(SEL_COMMAND, ID_ABOUT, :on_cmd_about)
      FXMAPFUNC(SEL_COMMAND, ID_CLEAR_HISTORY, :on_cmd_clear_history)
      FXMAPFUNC(SEL_COMMAND, ID_HELP, :on_cmd_help)
      FXMAPFUNC(SEL_COMMAND, ID_LICENSE, :on_cmd_license)

      @text_font = FXFont.new(getApp, 'helvetica', 18)
      @button_font = FXFont.new(getApp, 'helvetica', 16)

      # Menus, keep them here on top.
      menu_bar(nil, self, LAYOUT_SIDE_TOP | LAYOUT_FILL_X) do |menu|
        menu_pane(:filemenu, self) do |filemen|
          menu_command(nil, filemen, 'Clear history', self, ID_CLEAR_HISTORY, 0)
          menu_command(nil, filemen, 'Quit', getApp, FXApp::ID_QUIT, 0)
        end

        menu_pane(:aboutmenu, self) do |aboutmen|
          menu_command(nil, aboutmen, 'About', self, ID_ABOUT, 0)
          menu_command(nil, aboutmen, 'Help', self, ID_HELP, 0)
          menu_command(nil, aboutmen, 'License', self, ID_LICENSE, 0)
        end

        menu_title(nil, menu, 'File', filemenu)
        menu_title(nil, menu, 'About', aboutmenu)
      end

      status_bar(:statusbar, self, LAYOUT_SIDE_BOTTOM | LAYOUT_FILL_X | STATUSBAR_WITH_DRAGCORNER)

      vertical_frame(:calc_frame, self,
                     opts: LAYOUT_FILL_X | LAYOUT_FILL_Y | FRAME_THICK,
                     padTop: 10, padBottom: 10) do |frame|
        vertical_frame(
          nil, frame,
          opts: LAYOUT_FILL_X | LAYOUT_FILL_Y | FRAME_NONE,
          padTop: 10, padBottom: 10
        ) do |scroll_f|
          data_target(:history_text_data, '')
          text_area(
            :history_text_area, scroll_f, history_text_data, FXDataTarget::ID_VALUE,
            opts: LAYOUT_FILL_X | LAYOUT_FILL_Y | TEXT_READONLY
          ) do |field|
            field.font = @text_font
            # autoscroll on getting text.
            # Methods that append the text must be called with +notify: true+
            # for this to work.
            field.connect(SEL_INSERTED) do
              field.makePositionVisible(history_text_area.length)
            end
          end
        end

        data_target(:text_field_data, '')

        horizontal_frame(nil, frame, opts: LAYOUT_FILL_X | FRAME_NONE, padding: 0) do |fr|
          spring(nil, fr, LAYOUT_FILL_X, relw: 6, padRight: 5) do |spring|
            text_field(
              :text_input_field, spring, 32, text_field_data, FXDataTarget::ID_VALUE,
              height: 60, opts: LAYOUT_FILL_X | FRAME_SUNKEN | FRAME_THICK | TEXTFIELD_ENTER_ONLY
            ) do |field|
              field.font = @text_font

              field.connect(SEL_COMMAND) do
                on_equation_submit(history_text_area, field)
              end
            end
          end
          # Submit button
          spring(nil, fr, LAYOUT_FILL_X | LAYOUT_FILL_Y, relw: 1) do |spring|
            colored_button(nil, spring,
                           'Enter',
                           opts: LAYOUT_FILL_X | LAYOUT_FILL_Y | BUTTON_NORMAL) do |but|
              but.font = @button_font
              but.connect(SEL_COMMAND) do
                on_equation_submit(history_text_area, text_input_field)
              end
            end
          end
        end

        horizontal_frame(nil, frame,
                         opts: LAYOUT_FILL_X | FRAME_NONE,
                         padRight: 4, padLeft: 4) do |options|
          style = ['Simple parentheses', 'Square brackets', 'Curly brackets']

          popup(:pop, self)
          3.times do |idx|
            option(nil, pop, style[idx], opts: FRAME_THICK | JUSTIFY_HZ_APART | ICON_AFTER_TEXT)
              .connect(SEL_COMMAND) do
                on_parentheses_change(idx)
              end
          end

          horizontal_frame(
            nil, options,
            opts: LAYOUT_FILL_X | LAYOUT_FILL_Y | PACK_UNIFORM_WIDTH | FRAME_NONE
          ) do |splt|
            option_menu(nil, splt, pop,
                        opts: LAYOUT_FILL_X | LAYOUT_FILL_Y |
                        LAYOUT_TOP | FRAME_RAISED |
                        FRAME_THICK | JUSTIFY_HZ_APART | ICON_AFTER_TEXT)
          end

          horizontal_frame(nil, options,
                           opts: LAYOUT_FILL_X | LAYOUT_FILL_Y | PACK_UNIFORM_WIDTH | FRAME_NONE,
                           padding: 1) do |hframe|
            colored_button(:del, hframe, 'DEL', opts: button_layout) do |but|
              but.font = @button_font
              but.connect(SEL_COMMAND) do
                text_input_field.text = text_input_field.text.slice!(0..-2)
              end
            end

            colored_button(:ac, hframe, 'AC', opts: button_layout) do |but|
              but.font = @button_font
              but.connect(SEL_COMMAND) do
                text_input_field.text = ''
              end
            end
          end
        end

        horizontal_frame(
          nil, frame,
          opts: LAYOUT_FILL_X | LAYOUT_FILL_Y | PACK_UNIFORM_WIDTH | FRAME_NONE
        ) do |fr|
          matrix(nil, fr, 3, opts: matrix_layout) do |matr|
            [7, 8, 9, 4, 5, 6, 1, 2, 3, 0, ','].each do |label|
              operation_button(matr, label.to_s, matrix_button_layout)
            end

            operation_button(matr, 'EXP', matrix_button_layout) do |but|
              insert_on_button_click(but, text_input_field, '*10^(')
            end
          end

          matrix(nil, fr, 3, opts: matrix_layout) do |matr|
            operation_button(matr, '+', matrix_button_layout)

            ['(', ')'].each_with_index do |label, index|
              colored_button("parenthesis_button_#{index}".to_sym, matr,
                             label.to_s, opts: matrix_button_layout) do |but|
                but.font = @button_font

                insert_on_button_click(but, text_input_field, but.text.to_s)
              end
            end

            operation_button(matr, '-', matrix_button_layout)

            operation_button(matr, 'MOD', matrix_button_layout) do |but|
              insert_on_button_click(but, text_input_field, '%')
            end

            # With the root we kinda rely on user knowing how to work with the braces.
            operation_button(matr, '√', matrix_button_layout) do |but|
              # We can't use the UTF sign for root
              # because it contains more bytes and that will throw out of bounds exception
              # caused by the cursorPos on text field getting confused...
              insert_on_button_click(but, text_input_field, ')root(')
            end

            operation_button(matr, '*', matrix_button_layout)

            operation_button(matr, '^', matrix_button_layout)

            operation_button(matr, 'x!', matrix_button_layout) do |but|
              insert_on_button_click(but, text_input_field, '!')
            end

            operation_button(matr, '/', matrix_button_layout)

            operation_button(matr, '=', matrix_button_layout) do |but|
              but.connect(SEL_KEYRELEASE) do |_a, _b, c|
                on_equation_submit(history_text_area, text_input_field) if c.code == 32
                but.state = STATE_UP
              end
              but.connect(SEL_LEFTBUTTONRELEASE) do
                on_equation_submit(history_text_area, text_input_field)
                but.state = STATE_UP
                text_input_field.setFocus
              end
            end
          end
        end
      end
    end
    # rubocop: enable Metrics/MethodLength
    # rubocop: enable Metrics/AbcSize
    # rubocop: enable Metrics/BlockLength

    ##
    # Override of default create method.
    def create
      super
      show(PLACEMENT_SCREEN)
    end

    private

    ##
    # Container for common matrix layout.
    # @return [Integer] Layout value.
    def matrix_layout
      MATRIX_BY_COLUMNS | LAYOUT_FILL_X |
        LAYOUT_FILL_Y | FRAME_NONE |
        PACK_UNIFORM_WIDTH | PACK_UNIFORM_HEIGHT
    end

    ##
    # Container for common button layout.
    # @return [Integer] Layout value.
    def button_layout
      LAYOUT_FILL_X | LAYOUT_FILL_Y | BUTTON_NORMAL
    end

    ##
    # Container for common matrix button layout.
    # @return [Integer] Layout value.
    def matrix_button_layout
      button_layout | LAYOUT_FILL_COLUMN | LAYOUT_FILL_ROW
    end

    ##
    # Callback changing active parentheses on buttons based on passed index
    # @param index [Integer] Index of PARENTHESES to be used on the buttons,
    #   must be inside range of 0..2 including.
    # @raise [ArgumentError] if +index+ is out of range of 0..2 including.
    def on_parentheses_change(index)
      raise ArgumentError, 'Index out of range.' unless (0..2).include? index

      parenthesis_button_0.text = PARENTHESES[index][:left]
      parenthesis_button_1.text = PARENTHESES[index][:right]
    end

    ##
    # Callback creating the About dialog box.
    def on_cmd_about(_sender, _sel, _ptr)
      Calculator::GUI::AboutDialog.new(getApp).mbox.execute
    end

    ##
    # Callback clearing history in history text area.
    def on_cmd_clear_history(_sender, _sel, _ptr)
      statusbar.statusLine.text = 'Clearing calculator history.'
      history_text_area.setText('', true)
      statusbar.statusLine.text = 'Calculator history cleared.'
    end

    ##
    # Callback creating the help dialog
    def on_cmd_help(_sender, _sel, _ptr)
      return if Calculator::GUI::HelpDialog.new.execute

      state_box('Error', "User documentation not found in
                `#{Calculator::GUI::HelpDialog::USER_DOC_PATH}`")
    end

    ##
    # Callback creating the license dialog
    def on_cmd_license(_sender, _sel, _ptr)
      # This class works a bit different from other dialogs,
      # since it subclasses directly from FXDialogBox we can
      # simply call the execute without picking out the instance
      # as it is done in other dialogs.
      Calculator::GUI::LicenseDialog.new(getApp).execute
    end

    ##
    # Solve equation that was submit in +text_input_field+
    # and append it to the +text_area+
    #
    # @param text_area [Fox::FXText] Text area to append result to.
    # @param text_input_field [Fox::FXTextField] Text Field that the input gets taken from.
    def on_equation_submit(text_area, text_input_field)
      translator = Calculator::Translator.new(text_input_field.text)
      translated_expr = translator.translate.expression

      postfix_expr = parse_equation(translated_expr)

      return unless postfix_expr

      result = solve_equation(postfix_expr)

      return unless result

      output = big_decimal_to_s(result)
      append_text_from_field(text_area, text_input_field)
      append_text_to_area(text_area, "Result: #{output}")
      text_input_field.text = output
    end

    ##
    # Inserts +text+ at cursor position in +text_field+.
    #
    # After inserting the text, the cursor goes to the last position.
    #
    # @param text_field [Fox::FXTextField] Field where we want to insert the text.
    # @param text [String] Text that is to be inserted.
    def insert_on_cursor_pos(text_field, text)
      # Don't believe rubocop, the text field won't get inserted into unless we assign it...
      # rubocop: disable Style/RedundantSelfAssignment
      text_field.text = text_field.text.insert(text_field.cursorPos, text)
      # rubocop: enable Style/RedundantSelfAssignment
    end

    ##
    # Append text from FXTextField to FXText,
    # method takes in optional +notify+ argument that notifies the +text_field+ about changes.
    #
    # +text_input+ text attribute gets cleared.
    # The text is appended with newline.
    #
    # @param text_field [Fox::FXText] Field where we want to insert the text.
    # @param text_input [Fox::FXTextField] Text field that serves as input.
    # @param notify [Boolean] +true+ notifies the +text_field+ that text has been changed
    #   +false+ does not notify the +text_field+
    def append_text_from_field(text_field, text_input, notify: true)
      text_field.appendText("#{text_input.text}\n", notify)
      text_input.text = ''
    end

    ##
    # Append text to +text_area+, method takes in optional +notify+ argument that notifies the
    # +text_area+ about changes.
    #
    # The text is appended with newline.
    #
    # @param text_area [Fox::FXText] Field where we want to append the text.
    # @param text [String] String we want to append.
    # @param notify [Boolean] +true+ notifies the +text_area+ that text has been changed
    #   +false+ does not notify the +text_area+
    def append_text_to_area(text_area, text, notify: true)
      text_area.appendText("#{text}\n", notify)
    end

    ##
    # Function converting +BigDecimal+ to string.
    # The formats are either +22+ or +2.2+ or exponential format +0.22*10^(2)+.
    #
    # The exponentional form is used only when number of numerals exceeds 18 characters.
    #
    # @param big_decimal [BigDecimal] The +BigDecimal+ instance to be converted to string.
    # @return [String] String representation of the +big_decimal+.
    def big_decimal_to_s(big_decimal)
      str = big_decimal.to_s('F')

      if str.length > 18
        whole, exponent = big_decimal.to_s.split('e')
        return "#{whole[0..8]}*10^(#{exponent})"
      end

      whole, decimal = big_decimal.to_s('F').split('.')

      if decimal == '0'
        whole
      else
        "#{whole}.#{decimal}"
      end
    end

    ##
    # Parse +expression+ into postfix tokens, alternatively
    # display dialog box with error message if an exception occured.
    #
    # @param expression [String] Expression to be parsed.
    # @return [Array] Array of tokens in the postfix notation.
    def parse_equation(expression)
      Parser.new(expression).tokenize.to_postfix.tokens
    rescue Calculator::Parser::UnknownCharacter => e
      display_warning('Syntax error, unexpected character:', e.char)
    rescue Calculator::Parser::ParenthesisMatchError
      display_warning('Unmatched parentheses')
    rescue Calculator::Parser::ParserError
      display_error('Syntax error')
    end

    ##
    # Solve the postfix expression, alternatively
    # display dialog box with error message if an exception occured.
    #
    # @param postfix_expression [Array] postfix expression to solve.
    # @return [BigDecimal] Result of the expression.
    def solve_equation(postfix_expression)
      Solver.new(postfix_expression).solve
    rescue Calculator::Solver::SolverError => e
      if e.message.match? 'Illegal factorial operand value'
        display_warning('Factorial must be bigger than or equal to 0')
      else
        display_error("Can't solve the expression.")
      end
    end

    ##
    # Display Message box titled "Warning" with contents of +message+.
    #
    # Additional text arguments can be supplied as an array
    #
    # @param message [String] Message to be displayed.
    # @param args [Array] array of additional strings to be appended to +message+.
    # @return [nil]
    def display_warning(message, *args)
      text = message.capitalize.to_s

      args&.each do |val|
        text += " #{val}"
      end

      state_box('Warning', text)
    end

    ##
    # Display Message box titled "Error" with contents of +message+.
    #
    # Additional text arguments can be supplied as an array
    #
    # @param message [String] Message to be displayed.
    # @param args [Array] Array of additional strings to be appended to +message+.
    # @return [nil]
    def display_error(message, *args)
      text = message.to_s.capitalize

      args&.each do |val|
        text += " #{val}"
      end

      state_box('Error', text)
    end

    ##
    # Create a general button with an operation that inserts text into +App+'s +input_text_field+.
    # The button also receives font from +@button_font+ instance variable.
    #
    # Optionally define custom block with actions for the button.
    #
    # @param parent [Fox::FXComposite] parent frame
    # @param label [String] Button's text that also is inserted to the text field by default.
    # @param layout [Integer] Layout options for the button.
    # @return [Calculator::GUI::ColoredButton] Initialized colored button object.
    # @yield [colored_button, label]
    def operation_button(parent, label, layout)
      colored_button(nil, parent, label, opts: layout) do |but|
        but.font = @button_font

        if block_given?
          yield but, label
        else
          insert_on_button_click(but, text_input_field, but.text.to_s)
        end
      end
    end

    ##
    # Insert +text+ to +text_field+ on spacebar release and left mouse button release on +button+.
    #
    # @param button [Fox::FXButton] Button that the actions are registered for.
    # @param text_field [Fox::FXText] Text field the the text gets inserted to.
    # @param text [String] Text that gets appended.
    def insert_on_button_click(button, text_field, text)
      button.connect(SEL_KEYRELEASE) do |_a, _b, keyboard_event|
        # 32 == space
        insert_on_cursor_pos(text_field, text) if keyboard_event.code == 32
        button.state = STATE_UP
      end
      # Set focus back on input field if the button was clicked.
      button.connect(SEL_LEFTBUTTONRELEASE) do
        insert_on_cursor_pos(text_field, text)
        button.state = STATE_UP
        text_field.setFocus
      end
    end
  end
  # rubocop: enable Metrics/ClassLength
end
