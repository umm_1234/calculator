# Copyright (C) 2021 Jaroslav Prokop <jar.prokop@volny.cz>

# This file is part of Umm calculator.

# Umm calculator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Umm calculator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Umm calculator.  If not, see <https://www.gnu.org/licenses/>.

module Calculator
  ##
  # +Translator+ deals with dealing multi letter expressions into
  # form that parser understands.
  #
  # This allows more flexibility to the expressions as we don't need special
  # letters in GUI as they get translated here.
  #
  # @author Jaroslav Prokop
  class Translator
    # Mapping from multi letter words into form that parser understands.
    OPERATION = {
      root: 'r'
    }.freeze

    # The expression to get translated
    attr_reader :expression

    ##
    # @param expression [String] Expression that gets translated.
    def initialize(expression)
      @expression = expression
    end

    ##
    # Translate the expression
    # @return [String] The translated string
    def translate
      expr = @expression.clone

      OPERATION.each_pair do |key, val|
        expr.gsub!(key.to_s, val.to_s)
      end

      self.class.new expr
    end
  end
end
