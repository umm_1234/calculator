# Copyright (C) 2021 Jaroslav Prokop <jar.prokop@volny.cz>

# This file is part of Umm calculator.

# Umm calculator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Umm calculator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Umm calculator.  If not, see <https://www.gnu.org/licenses/>.
require 'calculator/translator'

RSpec.describe Calculator::Translator do
  let(:translator) { described_class.new(expression) }

  describe '#translate.expression' do
    context 'with root' do
      context 'with single word' do
        let(:expression) { 'root' }

        it 'substitutes it with the letter r' do
          expect(translator.translate.expression).to eq 'r'
        end
      end

      context 'with unclosed parentheses' do
        let(:expression) { ')root(' }

        it 'substitutes it with the letter r' do
          expect(translator.translate.expression).to eq ')r('
        end
      end

      context 'with closed parentheses' do
        let(:expression) { '()root()' }

        it 'substitutes it with the letter r' do
          expect(translator.translate.expression).to eq '()r()'
        end
      end

      context 'with arguments in parentheses' do
        let(:expression) { '(2)root(4)' }

        it 'substitutes it with the letter r' do
          expect(translator.translate.expression).to eq '(2)r(4)'
        end
      end

      context 'with expression before root' do
        let(:expression) { '8 * 5 - (2)root(4)' }

        it 'substitutes it with the letter r' do
          expect(translator.translate.expression).to eq '8 * 5 - (2)r(4)'
        end
      end

      context 'with expression after root' do
        let(:expression) { '(2)root(4) + 8 * 5' }

        it 'substitutes it with the letter r' do
          expect(translator.translate.expression).to eq '(2)r(4) + 8 * 5'
        end
      end

      context 'with expression around root' do
        let(:expression) { '5 + 5 - (2)root(4) + 8 * 9' }

        it 'substitutes it with the letter r' do
          expect(translator.translate.expression).to eq '5 + 5 - (2)r(4) + 8 * 9'
        end
      end

      context 'with multiple occurences' do
        let(:expression) { '()root() )root( * 8 root' }

        it 'substitutes all of them' do
          expect(translator.translate.expression).to eq '()r() )r( * 8 r'
        end
      end

      context 'with already translate.expressiond expression' do
        let(:expression) { '()r() )r( r * 8' }

        it 'does not change the expression' do
          expect(translator.translate.expression).to eq expression
        end
      end
    end
  end
end
