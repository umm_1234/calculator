# Copyright (C) 2021 Milan Tichavský <milantichavsky@seznam.cz>

# This file is part of Umm calculator.

# Umm calculator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Umm calculator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Umm calculator.  If not, see <https://www.gnu.org/licenses/>.

require 'calculator/parser'
require 'rspec'
require 'bigdecimal'

# For easier and more readable creation of certain structs from Calculator module.
module ParserHelper
  class << self
    def operation(opr)
      Calculator::Operation.new(opr)
    end

    def parenthesis(which)
      Calculator::Parenthesis.new(which)
    end
  end
end

RSpec.describe Calculator::Parser do
  describe '#tokenize' do
    context 'with decimal numbers' do
      it 'works (dot as separator)' do
        expression = '26.98 * 12.756'
        expecting = [BigDecimal('26.98'), ParserHelper.operation(:mul), BigDecimal('12.756')]
        result = described_class.new(expression).tokenize.tokens
        expect(result).to match expecting
      end

      it 'works (comma as separator)' do
        expression = '26,98 * 12,756'
        expecting = [BigDecimal('26.98'), ParserHelper.operation(:mul), BigDecimal('12.756')]
        result = described_class.new(expression).tokenize.tokens
        expect(result).to match expecting
      end
    end

    context 'with illegal number format of decimal numbers' do
      it 'raises an exception' do
        expression = '31..231'
        expect do
          described_class.new(expression).tokenize.tokens
        end.to raise_error('Cannot be converted to BigDecimal')
      end

      it 'raises an exception' do
        expression = '31.23,1'
        expect do
          described_class.new(expression).tokenize.tokens
        end.to raise_error('Cannot be converted to BigDecimal')
      end

      it 'raises an exception' do
        expression = '31231,,'
        expect do
          described_class.new(expression).tokenize.tokens
        end.to raise_error('Cannot be converted to BigDecimal')
      end
    end

    context 'with a mathematical expression in base 10' do
      it 'splits the expression into separate tokens' do
        expression = '2*305 /( 3 - 10)'
        expecting = [BigDecimal('2'), ParserHelper.operation(:mul), BigDecimal('305'),
                     ParserHelper.operation(:div), ParserHelper.parenthesis(:left), BigDecimal('3'),
                     ParserHelper.operation(:sub), BigDecimal('10'),
                     ParserHelper.parenthesis(:right)]

        expect(described_class.new(expression).tokenize.tokens).to match expecting
      end

      it 'splits even if it mathematically does not make sense' do
        expression = ') */- ('
        expecting = [ParserHelper.parenthesis(:right),
                     ParserHelper.operation(:mul), ParserHelper.operation(:div),
                     ParserHelper.operation(:ums), ParserHelper.parenthesis(:left)]
        expect(described_class.new(expression).tokenize.tokens).to match expecting
      end
    end

    context 'with an invalid mathematical expression' do
      context 'with an unknown character' do
        it 'raises an exception' do
          expression = '2x3'
          expect do
            described_class.new(expression).tokenize.tokens
          end.to raise_error('Unknown character')
        end
      end

      context 'with base 16 expression' do
        it 'raises an exception' do
          expression = '2*3A5 /( B - F)'
          expect do
            described_class.new(expression).tokenize.tokens
          end.to raise_error('Unknown character')
        end
      end
    end

    context 'unary minus' do
      it 'works' do
        expression = '- 7 + 2'
        expecting = [ParserHelper.operation(:ums), BigDecimal('7'), ParserHelper.operation(:add),
                     BigDecimal('2')]
        expect(described_class.new(expression).tokenize.tokens).to match expecting
      end
    end
  end

  describe '#to_postfix' do
    context 'with matching parentheses' do
      context 'with a simple expression' do
        it 'converts it to a corresponding postfix form' do
          expression = '35 + 12'
          expecting = [BigDecimal('35'), BigDecimal('12'), ParserHelper.operation(:add)]
          expect(described_class.new(expression).tokenize.to_postfix.tokens).to match expecting
        end

        it 'converts it to a corresponding postfix form (modulo)' do
          expression = '35 % 12'
          expecting = [BigDecimal('35'), BigDecimal('12'), ParserHelper.operation(:mod)]
          expect(described_class.new(expression).tokenize.to_postfix.tokens).to match expecting
        end
      end

      context 'with a more complex expression' do
        it 'converts it to a corresponding postfix form' do
          expression = '((18 - 69) - 58 * (44/10)) + 1'
          expecting = [BigDecimal('18'), BigDecimal('69'), ParserHelper.operation(:sub),
                       BigDecimal('58'), BigDecimal('44'), BigDecimal('10'),
                       ParserHelper.operation(:div), ParserHelper.operation(:mul),
                       ParserHelper.operation(:sub), BigDecimal('1'), ParserHelper.operation(:add)]
          expect(described_class.new(expression).tokenize.to_postfix.tokens).to match expecting
        end

        it 'converts it to a corresponding postfix form' do
          expression = '(25 + 5) * 3 + 20 / 2'
          expecting = [BigDecimal('25'), BigDecimal('5'), ParserHelper.operation(:add),
                       BigDecimal('3'), ParserHelper.operation(:mul), BigDecimal('20'),
                       BigDecimal('2'), ParserHelper.operation(:div), ParserHelper.operation(:add)]
          expect(described_class.new(expression).tokenize.to_postfix.tokens).to match expecting
        end

        it 'converts it to a corresponding postfix form' do
          expression = '4 + 3*(25 + 5)'
          result = described_class.new(expression).tokenize.to_postfix.tokens
          expecting = [BigDecimal('4'), BigDecimal('3'), BigDecimal('25'), BigDecimal('5'),
                       ParserHelper.operation(:add), ParserHelper.operation(:mul),
                       ParserHelper.operation(:add)]
          expect(result).to match expecting
        end
      end
    end

    context 'with parentheses that do NOT match' do
      context 'with an extra left parenthesis' do
        it 'raises an exception' do
          expression = '(( 10 - 2)'
          expect do
            described_class.new(expression).tokenize.to_postfix
          end.to raise_error('Matching right parenthesis is missing')
        end
      end

      context 'with an extra right parenthesis' do
        it 'raises an exception' do
          expression = '(10 - 2))'
          expect do
            described_class.new(expression).tokenize.to_postfix
          end.to raise_error('Matching left parenthesis is missing')
        end
      end

      context 'with a no matching parentheses' do
        it 'raises an exception' do
          expression = ')2('
          expect do
            described_class.new(expression).tokenize.to_postfix
          end.to raise_error('Matching left parenthesis is missing')
        end
      end
    end

    context 'with various types of brackets' do
      context 'with square brackets' do
        it 'converts the expression correctly' do
          expression = '36 * [9 - 8]'
          result = described_class.new(expression).tokenize.to_postfix.tokens
          expecting = [BigDecimal('36'), BigDecimal('9'), BigDecimal('8'),
                       ParserHelper.operation(:sub), ParserHelper.operation(:mul)]
          expect(result).to match expecting
        end
      end

      context 'with curly braces' do
        it 'converts the expression correctly' do
          expression = '{36 * {9 - 8}}'
          result = described_class.new(expression).tokenize.to_postfix.tokens
          expecting = [BigDecimal('36'), BigDecimal('9'), BigDecimal('8'),
                       ParserHelper.operation(:sub), ParserHelper.operation(:mul)]
          expect(result).to match expecting
        end
      end

      context 'with types of parentheses mixed in a single expression' do
        it 'converts it correctly to a postfix expression' do
          expression = '{5 / [1 - 2)]'
          result = described_class.new(expression).tokenize.to_postfix.tokens
          expecting = [BigDecimal('5'), BigDecimal('1'), BigDecimal('2'),
                       ParserHelper.operation(:sub), ParserHelper.operation(:div)]
          expect(result).to match expecting
        end

        it 'fails as it should (same behaviour as with only basic parentheses)' do
          expression = '[{ 5 + {1 - 2])'
          expect do
            described_class.new(expression).tokenize.to_postfix
          end.to raise_error('Matching right parenthesis is missing')
        end
      end
    end

    context 'with unary operations' do
      it 'works as intended (unary minus)' do
        expression = ' 7+-5'
        tokenize = described_class.new(expression).tokenize.tokens
        expect(tokenize).to match [BigDecimal('7'), ParserHelper.operation(:add),
                                   ParserHelper.operation(:ums), BigDecimal('5')]
        to_postfix = described_class.new(expression).tokenize.to_postfix.tokens
        expect(to_postfix).to match [BigDecimal('7'), BigDecimal('5'), ParserHelper.operation(:ums),
                                     ParserHelper.operation(:add)]
      end

      it 'works well' do
        expression = ' 7*5'
        to_postfix = described_class.new(expression).tokenize.to_postfix.tokens
        expect(to_postfix).to match [BigDecimal('7'), BigDecimal('5'), ParserHelper.operation(:mul)]
      end
    end

    context 'with more complex operations' do
      context 'with factorial' do
        it 'converts expression correctly' do
          expression = '5!'
          result = described_class.new(expression).tokenize.to_postfix.tokens
          expecting = [BigDecimal('5'), ParserHelper.operation(:fct)]
          expect(result).to match expecting
        end

        it 'converts expression correctly' do
          expression = '(5 + 3)!'
          result = described_class.new(expression).tokenize.to_postfix.tokens
          expecting = [BigDecimal('5'), BigDecimal('3'), ParserHelper.operation(:add),
                       ParserHelper.operation(:fct)]
          expect(result).to match expecting
        end

        it 'converts expression correctly' do
          expression = '(3! - 6) / 2 '
          result = described_class.new(expression).tokenize.to_postfix.tokens
          expecting = [BigDecimal('3'), ParserHelper.operation(:fct), BigDecimal('6'),
                       ParserHelper.operation(:sub), BigDecimal('2'), ParserHelper.operation(:div)]
          expect(result).to match expecting
        end
      end

      context 'with exponentation' do
        it 'converts expression correctly' do
          expression = '5^6'
          result = described_class.new(expression).tokenize.to_postfix.tokens
          expecting = [BigDecimal('5'), BigDecimal('6'), ParserHelper.operation(:exp)]
          expect(result).to match expecting
        end

        it 'converts expression correctly' do
          expression = '3 - 5^6!'
          result = described_class.new(expression).tokenize.to_postfix.tokens
          expecting = [BigDecimal('3'), BigDecimal('5'), BigDecimal('6'),
                       ParserHelper.operation(:fct), ParserHelper.operation(:exp),
                       ParserHelper.operation(:sub)]
          expect(result).to match expecting
        end
      end

      context 'with nth root' do
        it 'converts an expression correctly' do
          expression = '2r9 - (9)√(512)'
          result = described_class.new(expression).tokenize.to_postfix.tokens
          expecting = [BigDecimal('2'), BigDecimal('9'), ParserHelper.operation(:nrt),
                       BigDecimal('9'), BigDecimal('512'), ParserHelper.operation(:nrt),
                       ParserHelper.operation(:sub)]
          expect(result).to match expecting
        end
      end
    end
  end
end
