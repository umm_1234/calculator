# Copyright (C) 2021 Milan Tichavský <milantichavsky@seznam.cz>

# This file is part of Umm calculator.

# Umm calculator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Umm calculator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Umm calculator.  If not, see <https://www.gnu.org/licenses/>.

require 'calculator/solver'

RSpec.describe Calculator::Solver do
  describe '#solve' do
    context 'with an expression with integers and binary operations' do
      it 'solves it' do
        postfix = [BigDecimal('10'), BigDecimal('5'), Calculator::Operation.new(:add)]
        result = described_class.new(postfix).solve
        expect(BigDecimal('15')).to match result
      end

      it 'solves it (single number)' do
        postfix = [BigDecimal('37')]
        result = described_class.new(postfix).solve
        expect(BigDecimal('37')).to match result
      end

      it 'solves it (result negative)' do
        expression = '7 - 9 - 6'
        postfix = Calculator::Parser.new(expression).tokenize.to_postfix.tokens
        result = described_class.new(postfix).solve
        expect(BigDecimal('-8')).to match result
      end

      it 'solves it (result positive)' do
        expression = '7 + 9 / 3 * 5 - 2'
        postfix = Calculator::Parser.new(expression).tokenize.to_postfix.tokens
        result = described_class.new(postfix).solve
        expect(BigDecimal('20')).to match result
      end

      it 'solves it (result zero)' do
        expression = '(25 + 5) * 3 + 20 / 2 -100'
        postfix = Calculator::Parser.new(expression).tokenize.to_postfix.tokens
        result = described_class.new(postfix).solve
        expect(BigDecimal('0')).to match result
      end

      it 'solves it (with modulo)' do
        expression = '24 % 4 + 5 % 3'
        postfix = Calculator::Parser.new(expression).tokenize.to_postfix.tokens
        result = described_class.new(postfix).solve
        expect(BigDecimal('2')).to match result
      end
    end

    context 'with an expression with decimal numbers' do
      it 'solves it properly (dot as separator)' do
        expression = '2.5690 - 2.5'
        postfix = Calculator::Parser.new(expression).tokenize.to_postfix.tokens
        result = described_class.new(postfix).solve
        expect(result).to match BigDecimal('0.069')
      end

      it 'solves it properly (comma as separator)' do
        expression = '2,5690 - 2,5'
        postfix = Calculator::Parser.new(expression).tokenize.to_postfix.tokens
        result = described_class.new(postfix).solve
        expect(result).to match BigDecimal('0.069')
      end
    end

    context 'with an expression that does not make sense' do
      it 'raises an exception (parentesis)' do
        expression = [BigDecimal('39'), Calculator::Parenthesis.new(:left),
                      Calculator::Parenthesis.new(:right)]
        solver = described_class.new(expression)
        expect do
          solver.solve
        end.to raise_error('Invalid token')
      end

      it 'raises an exception (extra operation)' do
        expression = [BigDecimal('39'), BigDecimal('12'), Calculator::Operation.new(:mul),
                      Calculator::Operation.new(:div)]
        solver = described_class.new(expression)
        expect do
          solver.solve
        end.to raise_error('Stack is empty but it should not be.')
      end

      it 'raises an exception (infix expression)' do
        postfix = [BigDecimal('10'), Calculator::Operation.new(:add), BigDecimal('5')]
        solver = described_class.new(postfix)
        expect do
          solver.solve
        end.to raise_error('Stack is empty but it should not be.')
      end

      it 'raises an exception (unknown operation)' do
        postfix = [BigDecimal('10'), BigDecimal('5'), Calculator::Operation.new(:unk)]
        solver = described_class.new(postfix)
        expect do
          solver.solve
        end.to raise_error('Unknown operation')
      end

      it 'raises an exception (division by zero)' do
        postfix = [BigDecimal('10'), BigDecimal('0'), Calculator::Operation.new(:div)]
        solver = described_class.new(postfix)
        expect do
          solver.solve
        end.to raise_error('Cannot divide by zero')
      end

      it 'raises an exception (modulo with zero denominator)' do
        postfix = [BigDecimal('10'), BigDecimal('0'), Calculator::Operation.new(:mod)]
        solver = described_class.new(postfix)
        expect do
          solver.solve
        end.to raise_error('Modulo with zero denominator is illegal.')
      end
    end

    context 'with unary plus/minus operator' do
      context 'with minus at the beggining' do
        it 'solves the expression' do
          expression = '- 7 + 5'
          postfix = Calculator::Parser.new(expression).tokenize.to_postfix.tokens
          result = described_class.new(postfix).solve
          expect(BigDecimal('-2')).to match result
        end

        it 'solves the expression' do
          expression = '- (3 * 3 - 1)'
          postfix = Calculator::Parser.new(expression).tokenize.to_postfix.tokens
          result = described_class.new(postfix).solve
          expect(BigDecimal('-8')).to match result
        end

        it 'solves the expression' do
          expression = '(-3) ^ 2 + 2'
          postfix = Calculator::Parser.new(expression).tokenize.to_postfix.tokens
          result = described_class.new(postfix).solve
          expect(BigDecimal('11')).to match result
        end

        it 'solves the expression' do
          expression = '- 3 * 3 - 1'
          postfix = Calculator::Parser.new(expression).tokenize.to_postfix.tokens
          result = described_class.new(postfix).solve
          expect(BigDecimal('-10')).to match result
        end

        it 'solves it (testing operation priority)' do
          expression = '-2^4'
          postfix = Calculator::Parser.new(expression).tokenize.to_postfix.tokens
          result = described_class.new(postfix).solve
          expect(BigDecimal('-16')).to match result
        end
      end

      context 'with minus in the middle' do
        it 'solves it' do
          expression = '7 + (-5)'
          postfix = Calculator::Parser.new(expression).tokenize.to_postfix.tokens
          result = described_class.new(postfix).solve
          expect(BigDecimal('2')).to match result
        end

        it 'solves it' do
          expression = '5 - (- (- 6))'
          postfix = Calculator::Parser.new(expression).tokenize.to_postfix.tokens
          result = described_class.new(postfix).solve
          expect(BigDecimal('-1')).to match result
        end

        it 'solves it' do
          expression = '(2 * (-12)) / (-3)'
          postfix = Calculator::Parser.new(expression).tokenize.to_postfix.tokens
          result = described_class.new(postfix).solve
          expect(BigDecimal('8')).to match result
        end

        it 'solves it' do
          expression = '7 + - 5'
          postfix = Calculator::Parser.new(expression).tokenize.to_postfix.tokens
          result = described_class.new(postfix).solve
          expect(BigDecimal('2')).to match result
        end

        it 'solves the expression' do
          expression = '10 * - 10'
          postfix = Calculator::Parser.new(expression).tokenize.to_postfix.tokens
          result = described_class.new(postfix).solve
          expect(BigDecimal('-100')).to match result
        end
      end

      context 'with minus in an exponent' do
        it 'solves it' do
          expression = '1 + 2^(-1) + 2'
          postfix = Calculator::Parser.new(expression).tokenize.to_postfix.tokens
          result = described_class.new(postfix).solve
          expect(BigDecimal('3.5')).to match result
        end
      end

      context 'with unary plus' do
        it 'solves it' do
          expression = '7 - + 5'
          postfix = Calculator::Parser.new(expression).tokenize.to_postfix.tokens
          result = described_class.new(postfix).solve
          expect(BigDecimal('2')).to match result
        end

        it 'solves the expression' do
          expression = '10 * + 10'
          postfix = Calculator::Parser.new(expression).tokenize.to_postfix.tokens
          result = described_class.new(postfix).solve
          expect(BigDecimal('100')).to match result
        end

        it 'solves the expression' do
          expression = '+ (3 * 3 - 1)'
          postfix = Calculator::Parser.new(expression).tokenize.to_postfix.tokens
          result = described_class.new(postfix).solve
          expect(BigDecimal('8')).to match result
        end
      end
    end

    context 'with expression containing factorial' do
      it 'computes a simple factorial' do
        postfix = [BigDecimal('4'), Calculator::Operation.new(:fct)]
        result = described_class.new(postfix).solve
        expect(BigDecimal('24')).to match result
      end

      it 'solves it' do
        expression = '3 * 3! + 20'
        postfix = Calculator::Parser.new(expression).tokenize.to_postfix.tokens
        result = described_class.new(postfix).solve
        expect(BigDecimal('38')).to match result
      end

      it 'solves it (0! = 1)' do
        expression = '(3 - 3)! + 20'
        postfix = Calculator::Parser.new(expression).tokenize.to_postfix.tokens
        result = described_class.new(postfix).solve
        expect(BigDecimal('21')).to match result
      end

      it 'raises an exception as expected ((-1!) is undefined)' do
        expression = '2 - (5 - 6)!'
        postfix = Calculator::Parser.new(expression).tokenize.to_postfix.tokens
        solver = described_class.new(postfix)
        expect do
          solver.solve
        end.to raise_error('Illegal factorial operand value')
      end
    end

    context 'with an expression containing exponentation' do
      it 'computes simple exponentation' do
        postfix = [BigDecimal('2'), BigDecimal('3'), Calculator::Operation.new(:exp)]
        result = described_class.new(postfix).solve
        expect(BigDecimal('8')).to match result
      end

      it 'solves it correctly' do
        expression = '(2 + 3)^2! + 5'
        postfix = Calculator::Parser.new(expression).tokenize.to_postfix.tokens
        result = described_class.new(postfix).solve
        expect(BigDecimal('30')).to match result
      end

      it 'solves it correctly' do
        expression = '(0 - 4)^(1 - 2)'
        postfix = Calculator::Parser.new(expression).tokenize.to_postfix.tokens
        result = described_class.new(postfix).solve
        expect(BigDecimal('-0.25')).to match result
      end

      it 'solves it correctly' do
        expression = '(9)^(1/2)'
        postfix = Calculator::Parser.new(expression).tokenize.to_postfix.tokens
        result = described_class.new(postfix).solve
        expect(BigDecimal('3')).to match result
      end

      it 'solves it correctly' do
        expression = '(16)^(0-1/2)'
        postfix = Calculator::Parser.new(expression).tokenize.to_postfix.tokens
        result = described_class.new(postfix).solve
        expect(BigDecimal('0.25')).to match result
      end
    end

    context 'with nth root' do
      it 'solves it correctly' do
        expression = '2r9 - (9)√(512)'
        postfix = Calculator::Parser.new(expression).tokenize.to_postfix.tokens
        result = described_class.new(postfix).solve
        expect(result).to match BigDecimal('1')
      end

      it 'raises an exception as expected - negative root' do
        expression = '(-2)r9'
        postfix = Calculator::Parser.new(expression).tokenize.to_postfix.tokens
        solver = described_class.new(postfix)
        expect do
          solver.solve
        end.to raise_error('Illegal combination of parameters')
      end

      it 'raises an exception as expected - negative operand for even degree' do
        expression = '(8)r(-9)'
        postfix = Calculator::Parser.new(expression).tokenize.to_postfix.tokens
        solver = described_class.new(postfix)
        expect do
          solver.solve
        end.to raise_error('Illegal combination of parameters')
      end
    end
  end
end
