# Copyright (C) 2021 Ondřej Podroužek

# This file is part of Umm calculator.

# Umm calculator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Umm calculator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Umm calculator.  If not, see <https://www.gnu.org/licenses/>.

require 'calculator/math'
require 'bigdecimal'

RSpec.describe Calculator::Math do
  let(:our_math) { Calculator::Math.new }
  let(:precision) { 4 }

  describe '#unary_minus' do
    context 'with unary minus for BigDecimals' do
      it 'works (positive number)' do
        expect(our_math.unary_minus(BigDecimal('25'))).to match BigDecimal('-25')
      end

      it 'works (negative number)' do
        expect(our_math.unary_minus(BigDecimal('-25'))).to match BigDecimal('25')
      end

      it 'works (zero)' do
        expect(our_math.unary_minus(BigDecimal('0'))).to match BigDecimal('0')
      end
    end

    context 'with unary minus for Numerics' do
      it 'works (positive number)' do
        expect(our_math.unary_minus(25)).to match(-25)
      end

      it 'works (negative number)' do
        expect(our_math.unary_minus(-25)).to match 25
      end

      it 'works (zero)' do
        expect(our_math.unary_minus(0)).to match 0
      end
    end
  end

  describe '#modulo' do
    context 'with modulo operations' do
      it 'works with floats' do
        expect(our_math.modulo(5, 3)).to match 2
      end

      it 'works with Bigdecimals' do
        expect(our_math.modulo(BigDecimal('5'), BigDecimal('3'))).to match BigDecimal('2')
      end

      it 'works with negative numerator' do
        expect(our_math.modulo(-5, 3)).to match 1
      end

      it 'works with negative denominator' do
        expect(our_math.modulo(5, -3)).to match(-1)
      end

      it 'works with both numerator and denominator negative' do
        expect(our_math.modulo(-5, -3)).to match(-2)
      end

      it 'raises an exception when denominator zero' do
        expect do
          our_math.modulo(5, 0)
        end.to raise_error(StandardError, 'Modulo with zero denominator is illegal.')
      end
    end
  end

  describe '.addition' do
    context 'commutative check' do
      it ' is returning true' do
        a = BigDecimal(6, precision)
        b = BigDecimal(5, precision)
        placeholder = our_math.addition(b, a)
        expect(our_math.addition(a, b)).to eq placeholder
      end
    end

    context 'only positive numbers' do
      context 'with integer' do
        it 'is positive integer' do
          a = BigDecimal(6, precision)
          b = BigDecimal(5, precision)
          placeholder = BigDecimal(11, precision)
          expect(our_math.addition(a, b)).to eq placeholder
        end
      end
      context 'with float' do
        it 'is positive float' do
          a = BigDecimal(6.6, precision)
          b = BigDecimal(5.5, precision)
          placeholder = BigDecimal(12.1, precision)
          expect(our_math.addition(a, b)).to eq placeholder
        end
      end
    end

    context 'only negative numbers' do
      context 'with integer' do
        it 'is negative integer' do
          a = BigDecimal(-6, precision)
          b = BigDecimal(-5, precision)
          placeholder = BigDecimal(-11, precision)
          expect(our_math.addition(a, b)).to eq placeholder
        end
      end
      context 'with float' do
        it 'is negative float' do
          a = BigDecimal(-6.6, precision)
          b = BigDecimal(-5.5, precision)
          placeholder = BigDecimal(-12.1, precision)
          expect(our_math.addition(a, b)).to eq placeholder
        end
      end
    end

    context 'negative(a) and positive(b) numbers' do
      context 'with integer' do
        context 'where |b|<|a|' do
          it 'is negative integer' do
            a = BigDecimal(-6, precision)
            b = BigDecimal(5, precision)
            placeholder = BigDecimal(-1, precision)
            expect(our_math.addition(a, b)).to eq placeholder
          end
        end
        context 'where |b|>|a|' do
          it 'is positive integer' do
            a = BigDecimal(-5, precision)
            b = BigDecimal(6, precision)
            placeholder = BigDecimal(1, precision)
            expect(our_math.addition(a, b)).to eq placeholder
          end
        end
      end
      context 'with float' do
        context 'where |b|<|a|' do
          it 'is negative float' do
            a = BigDecimal(-6.6, precision)
            b = BigDecimal(5.5, precision)
            placeholder = BigDecimal(-1.1, precision)
            expect(our_math.addition(a, b)).to eq placeholder
          end
        end
        context 'where |b|>|a|' do
          it 'is positive float' do
            a = BigDecimal(-5.5, precision)
            b = BigDecimal(6.6, precision)
            placeholder = BigDecimal(1.1, precision)
            expect(our_math.addition(a, b)).to eq placeholder
          end
        end
      end
    end
  end

  describe '.subtraction' do
    context 'commutative check' do
      it ' is returning false' do
        a = BigDecimal(6, precision)
        b = BigDecimal(5, precision)
        placeholder = our_math.subtraction(b, a)
        expect(our_math.subtraction(a, b)).not_to eq placeholder
      end
    end

    context 'only positive numbers' do
      context 'with integer' do
        it 'is positive integer' do
          a = BigDecimal(6, precision)
          b = BigDecimal(5, precision)
          placeholder = BigDecimal(1, precision)
          expect(our_math.subtraction(a, b)).to eq placeholder
        end
      end
      context 'with float' do
        it 'is positive float' do
          a = BigDecimal(6.6, precision)
          b = BigDecimal(5.5, precision)
          placeholder = BigDecimal(1.1, precision)
          expect(our_math.subtraction(a, b)).to eq placeholder
        end
      end
    end

    context 'only negative numbers' do
      context 'with integer' do
        it 'is negative integer' do
          a = BigDecimal(-6, precision)
          b = BigDecimal(-5, precision)
          placeholder = BigDecimal(-1, precision)
          expect(our_math.subtraction(a, b)).to eq placeholder
        end
      end
      context 'with float' do
        it 'is negative float' do
          a = BigDecimal(-6.6, precision)
          b = BigDecimal(-5.5, precision)
          placeholder = BigDecimal(-1.1, precision)
          expect(our_math.subtraction(a, b)).to eq placeholder
        end
      end
    end

    context 'negative(a) and positive(b) numbers' do
      context 'with integer' do
        context 'where |b|<|a|' do
          it 'is negative integer' do
            a = BigDecimal(-6, precision)
            b = BigDecimal(5, precision)
            placeholder = BigDecimal(-11, precision)
            expect(our_math.subtraction(a, b)).to eq placeholder
          end
        end
        context 'where |b|>|a|' do
          it 'is negative integer' do
            a = BigDecimal(-5, precision)
            b = BigDecimal(6, precision)
            placeholder = BigDecimal(-11, precision)
            expect(our_math.subtraction(a, b)).to eq placeholder
          end
        end
      end
      context 'with float' do
        context 'where |b|<|a|' do
          it 'is negative float' do
            a = BigDecimal(-6.6, precision)
            b = BigDecimal(5.5, precision)
            placeholder = BigDecimal(-12.1, precision)
            expect(our_math.subtraction(a, b)).to eq placeholder
          end
        end
        context 'where |b|>|a|' do
          it 'is negative float' do
            a = BigDecimal(-5.5, precision)
            b = BigDecimal(6.6, precision)
            placeholder = BigDecimal(-12.1, precision)
            expect(our_math.subtraction(a, b)).to eq placeholder
          end
        end
      end
    end

    context 'negative(b) and positive(a) numbers' do
      context 'with integer' do
        context 'where |b|<|a|' do
          it 'is positive integer' do
            a = BigDecimal(6, precision)
            b = BigDecimal(-5, precision)
            placeholder = BigDecimal(11, precision)
            expect(our_math.subtraction(a, b)).to eq placeholder
          end
        end
        context 'where |b|>|a|' do
          it 'is positive integer' do
            a = BigDecimal(5, precision)
            b = BigDecimal(-6, precision)
            placeholder = BigDecimal(11, precision)
            expect(our_math.subtraction(a, b)).to eq placeholder
          end
        end
      end
      context 'with float' do
        context 'where |b|<|a|' do
          it 'is positive float' do
            a = BigDecimal(6.6, precision)
            b = BigDecimal(-5.5, precision)
            placeholder = BigDecimal(12.1, precision)
            expect(our_math.subtraction(a, b)).to eq placeholder
          end
        end
        context 'where |b|>|a|' do
          it 'is positive float' do
            a = BigDecimal(5.5, precision)
            b = BigDecimal(-6.6, precision)
            placeholder = BigDecimal(12.1, precision)
            expect(our_math.subtraction(a, b)).to eq placeholder
          end
        end
      end
    end
  end
  describe '.multiplication' do
    context 'commutative check' do
      it ' is returning true' do
        a = BigDecimal(6, precision)
        b = BigDecimal(5, precision)
        placeholder = our_math.multiplication(b, a)
        expect(our_math.multiplication(a, b)).to eq placeholder
      end
    end

    context 'only positive numbers' do
      context 'with integer' do
        it 'is positive integer' do
          a = BigDecimal(6, precision)
          b = BigDecimal(5, precision)
          placeholder = BigDecimal(30, precision)
          expect(our_math.multiplication(a, b)).to eq placeholder
        end
      end
      context 'with float' do
        it 'is positive float' do
          a = BigDecimal(6.6, precision)
          b = BigDecimal(5.5, precision)
          placeholder = BigDecimal(36.3, precision)
          expect(our_math.multiplication(a, b)).to eq placeholder
        end
      end
    end

    context 'only negative numbers' do
      context 'with integer' do
        it 'is positive integer' do
          a = BigDecimal(-6, precision)
          b = BigDecimal(-5, precision)
          placeholder = BigDecimal(30, precision)
          expect(our_math.multiplication(a, b)).to eq placeholder
        end
      end
      context 'with float' do
        it 'is positive float' do
          a = BigDecimal(-6.6, precision)
          b = BigDecimal(-5.5, precision)
          placeholder = BigDecimal(36.3, precision)
          expect(our_math.multiplication(a, b)).to eq placeholder
        end
      end
    end

    context 'negative(a) and positive(b) numbers' do
      context 'with integer' do
        context 'where |b|<|a|' do
          it 'is negative integer' do
            a = BigDecimal(-6, precision)
            b = BigDecimal(5, precision)
            placeholder = BigDecimal(-30, precision)
            expect(our_math.multiplication(a, b)).to eq placeholder
          end
        end
        context 'where |b|>|a|' do
          it 'is negative integer' do
            a = BigDecimal(-5, precision)
            b = BigDecimal(6, precision)
            placeholder = BigDecimal(-30, precision)
            expect(our_math.multiplication(a, b)).to eq placeholder
          end
        end
      end
      context 'with float' do
        context 'where |b|<|a|' do
          it 'is negative float' do
            a = BigDecimal(-6.6, precision)
            b = BigDecimal(5.5, precision)
            placeholder = BigDecimal(-36.3, precision)
            expect(our_math.multiplication(a, b)).to eq placeholder
          end
        end
        context 'where |b|>|a|' do
          it 'is negative float' do
            a = BigDecimal(-5.5, precision)
            b = BigDecimal(6.6, precision)
            placeholder = BigDecimal(-36.3, precision)
            expect(our_math.multiplication(a, b)).to eq placeholder
          end
        end
      end
    end
  end

  describe '.division' do
    context 'commutative check' do
      it ' is returning false' do
        a = BigDecimal(6, precision)
        b = BigDecimal(5, precision)
        placeholder = our_math.division(b, a)
        expect(our_math.division(a, b)).not_to eq placeholder
      end
    end

    context 'division by zero check' do
      it 'raises an exception' do
        a = BigDecimal(6, precision)
        b = BigDecimal(0, precision)
        expect do
          our_math.division(a, b)
        end.to raise_error(StandardError,
                           "Division by zero in: '#{a} / #{b}'")
      end
    end

    context 'only positive numbers' do
      context 'with integer' do
        it 'is positive' do
          a = BigDecimal(6, precision)
          b = BigDecimal(5, precision)
          placeholder = BigDecimal(1.2, precision)
          expect(our_math.division(a, b)).to eq placeholder
        end
      end
      context 'with float' do
        it 'is positive' do
          a = BigDecimal(6.6, precision)
          b = BigDecimal(5.5, precision)
          placeholder = BigDecimal(1.2, precision)
          expect(our_math.division(a, b)).to eq placeholder
        end
      end
    end

    context 'only negative numbers' do
      context 'with integer' do
        it 'is positive' do
          a = BigDecimal(-6, precision)
          b = BigDecimal(-5, precision)
          placeholder = BigDecimal(1.2, precision)
          expect(our_math.division(a, b)).to eq placeholder
        end
      end
      context 'with float' do
        it 'is positive' do
          a = BigDecimal(-6.6, precision)
          b = BigDecimal(-5.5, precision)
          placeholder = BigDecimal(1.2, precision)
          expect(our_math.division(a, b)).to eq placeholder
        end
      end
    end

    context 'negative(a) and positive(b) numbers' do
      context 'with integer' do
        context 'where |b|<|a|' do
          it 'is negative' do
            a = BigDecimal(-6, precision)
            b = BigDecimal(5, precision)
            placeholder = BigDecimal(-1.2, precision)
            expect(our_math.division(a, b)).to eq placeholder
          end
        end
        context 'where |b|>|a|' do
          it 'is negative' do
            a = BigDecimal(-3, precision)
            b = BigDecimal(6, precision)
            placeholder = BigDecimal(-0.5, precision)
            expect(our_math.division(a, b)).to eq placeholder
          end
        end
      end
      context 'with float' do
        context 'where |b|<|a|' do
          it 'is negative' do
            a = BigDecimal(-6.6, precision)
            b = BigDecimal(5.5, precision)
            placeholder = BigDecimal(-1.2, precision)
            expect(our_math.division(a, b)).to eq placeholder
          end
        end
        context 'where |b|>|a|' do
          it 'is negative' do
            a = BigDecimal(-3.3, precision)
            b = BigDecimal(6.6, precision)
            placeholder = BigDecimal(-0.5, precision)
            expect(our_math.division(a, b)).to eq placeholder
          end
        end
      end
    end
  end
end
