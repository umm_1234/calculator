# Copyright (C) 2021 Jan Valušek

# This file is part of Umm calculator.

# Umm calculator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Umm calculator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Umm calculator.  If not, see <https://www.gnu.org/licenses/>.

require 'calculator/math'
require 'bigdecimal'

# Tests all complex functions in Math library
# with both BigDecimal and non-BigDecimal inputs.
# Precision is set to 4.

RSpec.describe Calculator::Math do
  let(:our_math) { Calculator::Math.new }
  let(:precision) { 4 }

  describe '#factorial' do
    context 'with big decimal' do
      context 'with positive integers' do
        it 'counts factorial' do
          n = BigDecimal('5', precision)
          expect(our_math.factorial(n)).to eq BigDecimal('120', precision)
        end
      end

      context 'with zero' do
        it 'counts factorial' do
          n = BigDecimal('0', precision)
          expect(our_math.factorial(n)).to eq BigDecimal('1', precision)
        end
      end

      context 'with negative integers' do
        it 'raises an exception' do
          expect do
            n = BigDecimal('-5', precision)
            our_math.factorial(n)
          end.to raise_error(RangeError,
                             "factorial: The processed number can't be negative.")
        end
      end

      context 'with non-integers' do
        it 'raises an exception' do
          expect do
            n = BigDecimal('5.5644', precision)
            our_math.factorial(n)
          end.to raise_error(RangeError,
                             'factorial: The processed number must be integer.')
        end
      end
    end

    context 'without big decimal' do
      context 'with positive integers' do
        it 'counts factorial' do
          expect(our_math.factorial(5)).to eq 120
        end
      end

      context 'with zero' do
        it 'counts factorial' do
          expect(our_math.factorial(0)).to eq 1
        end
      end

      context 'with negative integers' do
        it 'raises an exception' do
          expect do
            our_math.factorial(-5)
          end.to raise_error(RangeError,
                             "factorial: The processed number can't be negative.")
        end
      end

      context 'with non-integers' do
        it 'raises an exception' do
          expect do
            our_math.factorial(5.56544)
          end.to raise_error(RangeError,
                             'factorial: The processed number must be integer.')
        end
      end
    end
  end

  describe '#exponentiation' do
    context 'with big decimal' do
      context 'with positive bases' do
        it 'counts exponentiation' do
          b = BigDecimal('2', precision)
          p = BigDecimal('3', precision)
          expect(our_math.exponentiation(b, p)).to eq BigDecimal('8', precision)
        end
      end

      context 'with power 0' do
        it 'counts exponentiation' do
          b = BigDecimal('56', precision)
          p = BigDecimal('0', precision)
          expect(our_math.exponentiation(b, p)).to eq BigDecimal('1', precision)
        end
      end

      context 'with negative bases' do
        it 'counts exponentiation' do
          b = BigDecimal('-5', precision)
          p = BigDecimal('2', precision)
          expect(our_math.exponentiation(b, p)).to eq BigDecimal('25', precision)
        end

        it 'counts exponentiation' do
          b = BigDecimal('-6', precision)
          p = BigDecimal('3', precision)
          expect(our_math.exponentiation(b, p)).to eq BigDecimal('-216', precision)
        end
      end

      context 'with non-integer bases' do
        it 'counts exponentiation' do
          b = BigDecimal('2.55', precision)
          p = BigDecimal('2', precision)
          expect(our_math.exponentiation(b, p)).to eq BigDecimal('6.5025', precision)
        end
      end
    end

    context 'without big decimal' do
      context 'with positive bases' do
        it 'counts exponentiation' do
          expect(our_math.exponentiation(2, 3)).to eq 8
        end
      end

      context 'with power 0' do
        it 'counts exponentiation' do
          expect(our_math.exponentiation(56, 0)).to eq 1
        end
      end

      context 'with negative bases' do
        it 'counts exponentiation' do
          expect(our_math.exponentiation(-5, 2)).to eq 25
        end

        it 'counts exponentiation' do
          expect(our_math.exponentiation(-6, 3)).to eq(-216)
        end
      end

      context 'with non-integer bases' do
        it 'counts exponentiation' do
          expect(our_math.exponentiation(2.55, 2).round(4)).to eq 6.5025
        end
      end
    end
  end

  describe '#root' do
    context 'with big decimal' do
      context 'with positive numbers' do
        it 'counts root' do
          n = BigDecimal('16', precision)
          d = BigDecimal('2', precision)
          expect(our_math.root(n, d)).to eq BigDecimal('4', precision)
        end

        it 'counts root' do
          n = BigDecimal('27', precision)
          d = BigDecimal('3', precision)
          expect(our_math.root(n, d).round(precision)).to eq BigDecimal('3', precision)
        end
      end

      context 'with negative numbers and odd degrees' do
        it 'counts root' do
          n = BigDecimal('-8', precision)
          d = BigDecimal('3', precision)
          expect(our_math.root(n, d).round(precision)).to eq BigDecimal('-2', precision)
        end
      end

      context 'with negative numbers and even degrees' do
        it 'raises an exception' do
          n = BigDecimal('-4', precision)
          d = BigDecimal('2', precision)
          expect do
            our_math.root(n, d)
          end.to raise_error(RangeError,
                             "root: The number can't be negative when even degree is set.")
        end
      end

      context 'with zero' do
        it 'counts root' do
          n = BigDecimal('0', precision)
          d = BigDecimal('4', precision)
          expect(our_math.root(n, d)).to eq BigDecimal('0', precision)
        end
      end

      context 'with negative degrees' do
        it 'raises an exception' do
          n = BigDecimal('4', precision)
          d = BigDecimal('-2', precision)
          expect do
            our_math.root(n, d)
          end.to raise_error(RangeError,
                             'root: The degree must be greater than zero.')
        end
      end

      context 'with zero as the degree' do
        it 'raises an exception' do
          n = BigDecimal('4', precision)
          d = BigDecimal('0', precision)
          expect do
            our_math.root(n, d)
          end.to raise_error(RangeError,
                             'root: The degree must be greater than zero.')
        end
      end
    end

    context 'without big decimal' do
      context 'with positive numbers' do
        it 'counts root' do
          expect(our_math.root(16, 2)).to eq 4
        end

        it 'counts root' do
          expect(our_math.root(27, 3)).to eq 3
        end
      end

      context 'with negative numbers and odd degrees' do
        it 'counts root' do
          expect(our_math.root(-8, 3)).to eq(-2)
        end
      end

      context 'with negative numbers and even degrees' do
        it 'raises an exception' do
          expect do
            our_math.root(-4, 2)
          end.to raise_error(RangeError,
                             "root: The number can't be negative when even degree is set.")
        end
      end

      context 'with zero' do
        it 'counts root' do
          expect(our_math.root(0, 4)).to eq 0
        end
      end

      context 'with negative degrees' do
        it 'raises an exception' do
          expect do
            our_math.root(4, -2)
          end.to raise_error(RangeError, 'root: The degree must be greater than zero.')
        end
      end

      context 'with zero as the degree' do
        it 'raises an exception' do
          expect do
            our_math.root(4, 0)
          end.to raise_error(RangeError, 'root: The degree must be greater than zero.')
        end
      end
    end
  end

  describe '#log' do
    context 'with big decimal' do
      context 'with positive numbers and bases' do
        it 'counts logarithm' do
          n = BigDecimal('8', precision)
          b = BigDecimal('2', precision)
          expect(our_math.log(n, b)).to eq BigDecimal('3', precision)
        end

        it 'counts logarithm' do
          n = BigDecimal('456', precision)
          b = BigDecimal('3', precision)
          expect(our_math.log(n, b).round(precision)).to eq BigDecimal('5.5729', precision)
        end
      end

      context 'with numbers less than or equal to zero' do
        it 'raises an exception' do
          n = BigDecimal('-4', precision)
          b = BigDecimal('2', precision)
          expect do
            our_math.log(n, b)
          end.to raise_error(RangeError,
                             'log: The number must be greater than zero.')
        end
      end

      context 'with bases less than or equal to zero' do
        it 'raises an exception' do
          n = BigDecimal('4', precision)
          b = BigDecimal('-2', precision)
          expect do
            our_math.log(n, b)
          end.to raise_error(RangeError,
                             'log: The base must be greater than zero.')
        end
      end

      context 'with base equal to one' do
        it 'raises an exception' do
          n = BigDecimal('4', precision)
          b = BigDecimal('1', precision)
          expect do
            our_math.log(n, b)
          end.to raise_error(RangeError,
                             "log: The base can't be equal to one.")
        end
      end
    end

    context 'without big decimal' do
      context 'with positive numbers and bases' do
        it 'counts logarithm' do
          expect(our_math.log(8, 2)).to eq 3
        end

        it 'counts logarithm' do
          expect(our_math.log(456, 3).round(precision)).to eq 5.5729
        end
      end

      context 'with numbers less than or equal to zero' do
        it 'raises an exception' do
          expect do
            our_math.log(-4, 2)
          end.to raise_error(RangeError,
                             'log: The number must be greater than zero.')
        end
      end

      context 'with bases less than or equal to zero' do
        it 'raises an exception' do
          expect do
            our_math.log(4, -2)
          end.to raise_error(RangeError,
                             'log: The base must be greater than zero.')
        end
      end

      context 'with base equal to one' do
        it 'raises an exception' do
          expect do
            our_math.log(4, 1)
          end.to raise_error(RangeError,
                             "log: The base can't be equal to one.")
        end
      end
    end
  end
end
