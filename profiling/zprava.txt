IVS - profiling (xpodro03_xproko44_xticha09_xvalus03)

In the attached HTML files you can see the output of profiling
with 10, 100, 1 000, and 1 000 000 numbers respectively. 
The numbers were generated randomly using random.org, 
in the range of 1 to 1 000 000 000. Instead of profiling 
only with the required numbers of numbers, I also choose 
to profile with 1 million numbers to have an idea of how 
fast the program is with a much bigger count of numbers. 
The profiler measured only the standard_deviation() function.


Our math library works with both standard and BigDecimal 
numerical types. After a discussion with my teammates, 
we agreed that we should profile with BigDecimal numbers 
since they give us more precise results and allow us to use
bigger values. The program would be much faster when using 
standard data types but there would be a risk of overflowing etc.


The program spends most of its time in functions 
related to the BigDecimal library. That's something we can't optimize 
but we could look for similar libraries that would allow us operations 
with big values but also would be faster than BigDecimal.


Just a fraction of time is spent in other parts of our math library.
