#!/bin/bash
set -xe

version=$1
shift

name="umm-calculator"

# Create archive
cd src
make udoc
make archive VERSION="${version}"
cp ../"${name}-${version}.tar.xz" ../installer/

# Build the package
cd ../installer
tar -xvf "umm-calculator-${version}.tar.xz"
dpkg-buildpackage -rfakeroot -uc -b

mv "../${name}_${version}-1_all.deb" ./
