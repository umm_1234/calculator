# Calculator project [![coverage report](https://gitlab.com/umm_1234/calculator/badges/master/coverage.svg)](https://umm_1234.gitlab.io/calculator) [![pipeline status](https://gitlab.com/umm_1234/calculator/badges/master/pipeline.svg)](https://gitlab.com/umm_1234/calculator/-/pipelines?&scope=branches&ref=master)

## Development setup

See
[CONTRIBUTING.md](https://gitlab.com/umm_1234/calculator/-/blob/master/CONTRIBUTING.md)
for more info about the suggested workflow.

First, make sure that you have development libraries installed to be able to
install the native extensions of
[FXRuby](https://github.com/larskanis/fxruby#label-INSTALL-3A).

### Installing FXRuby dependencies

FXRuby is a wrapper for the FOX GUI library and as such it uses native
extensions, you need to install proper development libraries for your OS.

#### Fedora

`sudo dnf install -y fox-devel libXrandr-devel libXcursor-devel g++
redhat-rpm-config ruby-devel`

#### Ubuntu

`sudo apt-get install g++ libxrandr-dev libx11-dev libxcursor-dev libfox-1.6-dev
ruby-full`

### Installing development dependencies

We are using SSH here, if you choose this option, you can have
[passwordless](https://docs.gitlab.com/ee/ssh/README.html) access to the
repository.

In the other case, if you do not wish to use or cannot install using SSH, you
can use the clone over HTTP instead with the following link:
https://gitlab.com/umm_1234/calculator.git

```
git clone git@gitlab.com:umm_1234/calculator.git
cd src/calculator
bundle config set path './vendor'
bundle install # Note that this can take a while since it is compiling native
               # extensions.
```
------

If you are still having a problem, follow FXRuby's guide on setting up [build
environment](https://github.com/lylejohnson/fxruby/wiki/Setting-Up-a-Linux-Build-Environment).

Alternatively, [submit an
issue](https://gitlab.com/umm_1234/calculator/-/issues/new?issue) describing
your problem.

## Installation setup

To install the application manually on your system instead of using the
[generated deb package](https://gitlab.com/umm_1234/calculator/-/jobs/1223835760/artifacts/raw/public/installer/umm-calculator_1.0.0-1_all.deb)
you will need to follow the [Installing FXRuby dependencies
](https://gitlab.com/umm_1234/calculator#installing-fxruby-dependencies) step.

### Ubuntu
Next step is installing build dependencies, this step presumes you installed FXRuby dependencies:
```
$ sudo apt install make yard
$ sudo apt install texlive-lang-czechslovak texlive-latex-base \
  texlive-fonts-recommended texlive-fonts-extra texlive-latex-extra \
  ruby-redcloth # OPTIONAL, only install these if you want to regenerate the
                # user documentation.
```

With these dependencies you are ready to run the make steps.
```
$ make udoc # Only run this if you have the latex dependencies installed
$ make install DESTDIR=/path/to/you/dest RUBY_SITELIB=/path/to/vendor_ruby
```

## Uninstalling

To uninstall you simply run
```
$ make uninstall DESTDIR=/path/to/your/dest RUBY_SITELIB=/path/to/vendor_ruby
```

## Running tests

To run tests the [development
dependencies](https://gitlab.com/umm_1234/calculator#installing-development-dependencies)
must be installed.  We are using the
[RSpec](https://github.com/rspec/rspec-core) testing suite and
[Rubocop](https://github.com/rubocop/rubocop/) linter to unify code style.

To run only RSpec tests execute
  * `bundle exec rake spec`
  * or you can call RSpec directly `bundle exec rspec spec`

To run only Rubocop execute
  * `bundle exec rake rubocop`

To run all tests and linting execute
  * `bundle exec rake validate`

To make Rubocop automatically correct offenses for you
  * `bundle exec rubocop -a`

## Environment

Ubuntu 64bit

## Authors

Umm_1234

- xproko44 Jaroslav Prokop
- xticha09 Milan Tichavský
- xvalus03 Jan Valušek
- xpodro03 Ondřej Podroužek

## Licence

Copyright (C) 2021  Jaroslav Prokop, Jan Valušek, Milan Tichavský, Ondřej
Podroužek

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <https://www.gnu.org/licenses/>.
