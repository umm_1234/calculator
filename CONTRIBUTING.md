# Contributing the code

First of all, follow the development setup in README.md file.

### Create a new branch

with `git checkout -b [branch_name]`
    - shorthand for creating a new branch (`git branch [name]`) and checkout to
      it (`git checkout [name]`)

### Do the changes

As you work on this branch, commit all necessary changes along the way. With
each commit, make sure

- you wrote automated tests for your change (if possible)
- that all tests including Rubocop are green
    - `bundle exec rake validate` tests and linter (preferred)
    - For more information about executing tests, see
      [README.md](https://gitlab.com/umm_1234/calculator#running-tests).
- to write a reasonable [commit
  message](https://chris.beams.io/posts/git-commit/).
- the commit is atomic (changes one logical thing)

The library files should be in `src/calculator/lib/`, tests in
`src/calculator/specs/` (we are using `rspec` framework for tests).

Keep your local repository updated by running commands `git pull` (fetch and
merge), or `git pull --rebase` (even though `fast forward` should be good enough
most of the time). You usually want to download changes from the `origin`
remote.

Don't forget to distinguish between local branches (that you can work on) and
remote branches (you cannot commit to them directly), keep your local master
branch is in sync with its remote counterpart and don't force push into the
`origin/master`.

### Submit changes for code review

To **set things up**, go to GitLab project page and fork the project (there's a
button for it). Add the remote to your repo by running `git remote add [my_fork]
[url address of the fork]`.  Now you can download and upload changes to/from two
different servers - `origin` and `my_fork`, e.g., by running `git push my_fork`
you upload changes of your current branch to the `my_fork` remote repository.
You can run `git remote -v` to verify that you added the remote correctly.

Now you upload the branch to the forked repository `git push [my_fork]
[my_branch]`. You can use `-u` to set this repo as upstream for push.

To create the pull request, go to you fork's Gitlab page, choose the branch you
wanna merge from the dropdown menu, and the button `Create merge request` will
show up. Click on it and fill in the details in the form, confirm and you should
be done.

You can label your issue as `WIP:` or `Draft:` to indicate work-in-progress.
Linking issues is done through hash sign(#) and number(4) e.g. #11 , you can
link similarly merge requests with bang and number, !3.

# Submitting a bug

Open an Issue with `bug` label, describing all the necessary steps to reproduce
the bug and information about your environment by running `gem list` and `ruby
--version`.
