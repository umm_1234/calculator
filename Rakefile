# Copyright (C) 2021 Jaroslav Prokop <jar.prokop@volny.cz>

# This file is part of Umm calculator.

# Umm calculator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Umm calculator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Umm calculator.  If not, see <https://www.gnu.org/licenses/>.

require 'yard'
require 'rspec/core/rake_task'
require 'rubocop/rake_task'

RSpec::Core::RakeTask.new(:spec) do |t|
  cd 'src'
end

RuboCop::RakeTask.new(:rubocop)

YARD::Rake::YardocTask.new(:doc) do |t|
  t.files   = ['src/lib/**/*.rb', '-', 'LICENSE.txt', 'CONTRIBUTING.md']
  t.options = ['--protected', '--private', '--readme', 'README.md']
  t.stats_options = ['--list-undoc']
end

task default: :spec

desc 'Run rspec and rubocop'
task validate: [:spec, :rubocop]

desc 'Generate private program documentation'
task document: :doc
